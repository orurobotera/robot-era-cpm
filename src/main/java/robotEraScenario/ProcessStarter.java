package robotEraScenario;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;


public class ProcessStarter {

	public static void main(String args[]) throws Exception {
		File dir = new File("scripts");
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".fe");
			}
		});

		for (File file : files) {
			int ret = exec(FakeExecutor.class, file.getAbsolutePath());
			System.out.println("Started process for " + file.getAbsolutePath() + " result: " + ret);
		}

	}

	public static int exec(Class klass, String ... arguments) throws IOException, InterruptedException {
		String javaHome = System.getProperty("java.home");
		String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
		String classpath = System.getProperty("java.class.path");
		String className = klass.getCanonicalName();
		
		String[] realArgs = new String[arguments.length+4];
		realArgs[0] = javaBin;
		realArgs[1] = "-cp";
		realArgs[2] = classpath;
		realArgs[3] = className;
		for (int i = 0; i < arguments.length; i++) realArgs[i+4] = arguments[i];

		final ProcessBuilder builder = new ProcessBuilder(realArgs);

		new Thread() {
			public void run() {
				try {
				Process process = builder.start();
				while(true) { Thread.sleep(10); }
				}
				catch (InterruptedException e) { e.printStackTrace(); }
				catch (IOException e) { e.printStackTrace(); }
			}
		}.start();

		return 0;//process.exitValue();
	}


}
