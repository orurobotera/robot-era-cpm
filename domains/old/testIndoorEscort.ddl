##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   SimpleDomain                                                #
#   Constraint                                                  #
#   RequiredState						                        #
#   AchievedState						                        #
#   RequiredResource						                    #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain TestIndoorEscort)

(Sensor Bed)
(Sensor Toilet)
(Sensor TimeOfDay)

(ContextVariable HumanSleep)
(ContextVariable HumanState)

(Actuator Robot)
(Actuator Light)

(SimpleOperator
 (Head HumanSleep::Awake())
 (RequiredState req1 Bed::Off())
 (RequiredState req2 Bed::On())
 (RequiredState req3 TimeOfDay::Night())
 (RequiredState req4 RobotLocation::AtBedside())
 (RequiredState req5 RobotLocation::InToilet())
 (Constraint MetBy(req1,req2))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Finishes(Head,req1))
 (Constraint After(req5,req4))
)

(SimpleOperator
 (Head RobotLocation::AtBedside())
 (RequiredState req1 Robot::MoveTo())
 (RequiredState req2 Light::On())
 (Constraint Duration[3000,INF](req1))
 (Constraint Meets(req1,Head))
)

(SimpleOperator
 (Head RobotLocation::InToilet())
 (RequiredState req1 RobotLocation::AtBedside())
 (RequiredState req2 Robot::MoveTo())
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint After(req2,req1))
 (Constraint Meets(req2,Head))
)

(SimpleOperator
 (Head HumanState::Happy())
 (RequiredState req1 Toilet::On())
 (RequiredState req2 Toilet::Off())
 (RequiredState req3 RobotLocation::AtBedside())
 (Constraint Meets(req1,req2))
 (Constraint Finishes(req2,Head))
)


(Resource RobotMobility 1)

(SimpleOperator
 (Head Robot::MoveTo())
 (RequiredResource RobotMobility(1))
)