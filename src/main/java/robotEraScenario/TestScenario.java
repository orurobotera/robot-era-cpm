/*******************************************************************************
 * Copyright (c) 2010-2013 Federico Pecora <federico.pecora@oru.se>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/
package robotEraScenario;

import core.*;
import meta.configurationPlanner.RobotEraDispatchingFunction;
import meta.configurationPlanner.RobotEraSimplePlannerInferenceCallback;

import org.metacsp.dispatching.DispatchingFunction;
import org.metacsp.meta.simplePlanner.ProactivePlanningDomain;
import org.metacsp.meta.simplePlanner.SimpleDomain;
import org.metacsp.meta.simplePlanner.SimplePlanner;
import org.metacsp.multi.activity.ActivityNetworkSolver;
import org.metacsp.sensing.ConstraintNetworkAnimator;
import org.metacsp.sensing.Sensor;
import org.metacsp.time.Bounds;
import org.metacsp.utility.logging.MetaCSPLogging;
import org.metacsp.utility.timelinePlotting.TimelinePublisher;
import org.metacsp.utility.timelinePlotting.TimelineVisualizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestScenario {

	public static int CPM_ID = 995;
	public static int DORO_ID = 999;
	public static int CORO_ID = 998;
	//public static int CORO_ID = 1998;
	public static int DUSTCART_ID = 997;
	public static int LIFT_ID = 996;
    //public static int LIFT_ID = 1996;
	
	public static int CAM_ID = 9898;
	public static int HRI_ID = 42;
	public static final long PERIOD = 1000;

	public static SimpleDomain domain;
	public static LinkedList <String> timelineList;
	public static ConstraintNetworkAnimator animator;
	private static Logger logger = MetaCSPLogging.getLogger(TestScenario.class);


//	public static String originalSensorValue = null;
	
	private static void printUsage() {
		String yourPath = "domains";
		File directory = new File(yourPath);
		String[] myFiles = directory.list(new FilenameFilter() {
		    public boolean accept(File directory, String fileName) {
		        return fileName.endsWith(".ddl");
		    }
		});
		String[] newFileNames = new String[myFiles.length];
		for (int i = 0; i < newFileNames.length; i++) newFileNames[i] = myFiles[i].replace(".ddl", "");
		logger.info("Usage: ./gradlew run -PappArgs=\"['<domain_name>','<forget>']\", where:\n\t<domain_name> is one of " + Arrays.toString(newFileNames) + "\n\t<forget> (optional) is either true or false and specifies whether the CPM should use the forgetting mechanism (default = true)");		
	}
	
	public static void main(String[] args) {

		String[] program_id={"--peis-id", ("" + CPM_ID)};
        // TODO: read from args and if requested set a separate network ("--peis-network julien")
        // String[] program_id={"--peis-id", ("" + CPM_ID), network_name };
		try {PeisJavaMT.peisjava_initialize(program_id);}
		catch (WrongPeisKernelVersionException e1) {
			e1.printStackTrace();
			System.exit(1);
		}

		long origin = Calendar.getInstance().getTimeInMillis();
		//Create planner
		final SimplePlanner planner = new SimplePlanner(origin,origin+100000000,0);
		MetaCSPLogging.setLevel(planner.getClass(), Level.OFF);

		if (args.length == 0) {
			logger.info("***************************");
		    logger.info("ERROR: No domain specified!");
		    printUsage();
			logger.info("***************************");
		    System.exit(0);
		}
		
		String domainFilename = "domains" + File.separator + args[0] + ".ddl";
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(domainFilename));
			br.close();
		}
		catch(FileNotFoundException e) {
			logger.info("***************************");
		    logger.info("ERROR: Domain 'domains" + File.separator + args[0] + ".ddl' not found!");
		    printUsage();
			logger.info("***************************");
		    System.exit(0);
		} catch (IOException e) { e.printStackTrace(); }

		logger.info("==== Using domain: " + domainFilename + " ====");

		domain = SimpleDomain.parseDomain(planner, domainFilename, ProactivePlanningDomain.class);

		boolean forget = true;
		if (args.length == 2) {
			forget = Boolean.parseBoolean(args[1]);
		}
		final ActivityNetworkSolver ans = (ActivityNetworkSolver)planner.getConstraintSolvers()[0];
		MetaCSPLogging.setLevel(ans.getClass(), Level.OFF);
		final RobotEraSimplePlannerInferenceCallback cb = new RobotEraSimplePlannerInferenceCallback(planner, args[0], forget);
		animator = new ConstraintNetworkAnimator(ans, PERIOD, cb);
		MetaCSPLogging.setLevel(animator.getClass(), Level.OFF);

        timelineList = new LinkedList<String>();
        for (final String timeline : domain.getTimelines()) {
            timelineList.add(timeline);
        }


		final HashMap<String,String> currentSensorValues = new HashMap<String, String>();
		
		// Subscribing to PEIS tuples corresponding to sensors
        // Add sensor to the list of timelines
		for (final String sensor : domain.getSensors()) {
			final Sensor metaCSPSensor = new Sensor(sensor, animator);
			
			if (sensor.startsWith("in.")) {
				PeisJavaMT.peisjava_setMetaTuple(CPM_ID, sensor, -1, null);
				logger.info("GOT META SENSOR " + sensor);
				PeisJavaMT.peisjava_subscribeIndirectly(CPM_ID, sensor);
				
				
//				PeisJavaMT.peisjava_registerMetaTupleCallback(CPM_ID, sensor, new CallbackObject() {
//					
//					@Override
//					public void callback(PeisTuple sensor_tuple) {
//						System.out.println("****************************");
//						System.out.println("AH THE CALLBACK HAPPENS!!!!!");
//						System.out.println("****************************");
//					/*
//						String sensorValue = sensor_tuple.getStringData();
//						metaCSPSensor.postSensorValue(sensorValue, Calendar.getInstance().getTimeInMillis());
//						currentSensorValues.put(sensor,sensorValue);
//						System.out.println("Callback, got " + sensorValue);
//						System.out.println("[" + metaCSPSensor.getName() + "] Posted sensor reading: " + sensorValue);
//						*/
//					}
//				});
				
				// OLD METHOD. DEPRECATED.
				Thread t = new Thread() {
					public void run() {
						String originalSensorValue = null;
						String oldSensorValue = "";
						while (true) {
							PeisTuple ret = PeisJavaMT.peisjava_getTupleIndirectly(CPM_ID, sensor);
							if (ret != null) {
								String sensorValue = ret.getStringData();
								originalSensorValue = sensorValue;
								long timeNow = Calendar.getInstance().getTimeInMillis();
								if (sensor.equals("in.goal.type")) {
									String[] splitGoal = sensorValue.split(" ");
									sensorValue = splitGoal[0];
									String[] arguments = new String[splitGoal.length-1];
									for (int i = 1; i < splitGoal.length; i++) {
										arguments[i-1] = splitGoal[i];
									}
									cb.setGoalArguments(Sensor.getHashCode(sensor, sensorValue, timeNow), arguments);
								}
								if(!oldSensorValue.equals(originalSensorValue)) {
									metaCSPSensor.postSensorValue(sensorValue, timeNow);
									currentSensorValues.put(sensor,originalSensorValue);
									logger.info("THREAD METHOD, got " + originalSensorValue);
									logger.info("Posted sensor reading to " + metaCSPSensor.getName() + ": " + sensorValue);
								}
								oldSensorValue = originalSensorValue;
							}
							try { Thread.sleep(PERIOD);	}
							catch (InterruptedException e) { e.printStackTrace(); }
						}
					}
				};
				t.start();
			}
			else {
				PeisJavaMT.peisjava_subscribe(CAM_ID, sensor);
				PeisJavaMT.peisjava_registerTupleCallback(CAM_ID,sensor,new CallbackObject() {
					@Override
					public void callback(PeisTuple tuple) {
						String sensorValue = tuple.getStringData();
						metaCSPSensor.postSensorValue(sensorValue, Calendar.getInstance().getTimeInMillis());
						currentSensorValues.put(sensor,sensorValue);
						logger.info("Posted sensor reading to " + metaCSPSensor.getName() + ": " + sensorValue);
					}
				});
			}
			
		}


        if (timelineList.size() > 0) {
            String[] sensorsActuatorsArray = timelineList.toArray(new String[timelineList.size()]);
            TimelinePublisher tp = new TimelinePublisher(ans.getConstraintNetwork(), new Bounds(0,60000), true,
                sensorsActuatorsArray );
		    TimelineVisualizer tv = new TimelineVisualizer(tp);

		    tv.startAutomaticUpdate(1000);  // frequency for plan monitoring
        }
		
		int componentID = 0;

		for (final String deviceName : domain.getActuators()) {

			if(deviceName.startsWith("doro")) { componentID = DORO_ID; }
			else if(deviceName.startsWith("coro")) { componentID = CORO_ID; }
			else if(deviceName.startsWith("dustcart")) { componentID = DUSTCART_ID; }
			else if(deviceName.startsWith("elevator")) { componentID = LIFT_ID; }
			else if(deviceName.startsWith("ws")) {componentID = HRI_ID; }
            DispatchingFunction df = new RobotEraDispatchingFunction(deviceName, ans, componentID,TestScenario.CPM_ID);
			animator.addDispatchingFunctions(ans, df);
			
		}
		


	}



}
