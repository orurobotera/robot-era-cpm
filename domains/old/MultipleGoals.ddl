##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain MultipleGoals)


# sensor tuples on CAM
(Sensor user1.pos.geo)
(Sensor in.goal.type)
(Sensor in.goal.args)
(Sensor water1.pos.geo)

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)

# Robots
(Actuator doro)

# Robot doro is a resource with limited capacity of one unit
(Resource doro_resource 1)


# PLANNING OPERATORS used for inferring desired goals and planning

(SimpleOperator
 (Head doro::moveto(WATER))
 (RequiredResource doro_resource(1))
)
(SimpleOperator
 (Head doro::moveto(USER))
 (RequiredResource doro_resource(1))
)
(SimpleOperator
 (Head doro::moveto(CORO))
 (RequiredResource doro_resource(1))
)
(SimpleOperator
 (Head doro::pickup(FOOD))
 (RequiredResource doro_resource(1))
)
(SimpleOperator
 (Head doro::moveto(USER))
 (RequiredResource doro_resource(1))
)
(SimpleOperator
 (Head doro::pickup(WATER))
 (RequiredResource doro_resource(1))
)
(SimpleOperator
 (Head doro::handover(USER))
 (RequiredResource doro_resource(1))
)

# bring an object to be handed over to the human user with the help of
#  doro
(SimpleOperator
 (Head inference.goal.type::BRING)
 (RequiredState req0 in.goal.type::BRING)
 (RequiredState req1 doro::moveto(WATER))
 (RequiredState req2 doro::pickup(WATER))
 (RequiredState req3 doro::moveto(USER))
 (RequiredState req4 doro::handover(USER))
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3,req4))
 (Constraint Starts[10,10](Head,req0))
)

# doro goes to the user and interacts with them
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(USER))
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req0))
 (Constraint Duration[3000,INF](req1))
 (Constraint Starts[10,10](Head,req0))
)

(SimpleOperator
 (Head inference.goal.type::FOOD)
 (RequiredState req0 in.goal.type::FOOD)
 (RequiredState req1 doro::moveto(CORO))
 (RequiredState req2 doro::pickup(FOOD))
 (RequiredState req3 doro::moveto(USER))
 (RequiredState req4 doro::handover(USER))
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3, req4))
 (Constraint Starts[10,10](Head,req0))
)