##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain FoodDelivery)


# sensor tuples on CAM
(Sensor in.goal.type)
(Sensor in.goal.args)

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)

# Robots and web server
(Actuator doro)
(Actuator coro)
(Actuator ws)


# PLANNING OPERATORS used for inferring desired goals and planning

# doro goes to the user and interacts with them
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)

 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req4,req_res_doro))

)

# food delivery with the help of doro and coro
(SimpleOperator
 (Head inference.goal.type::BRING_FOOD)

 (RequiredState req0 in.goal.type::BRING_FOOD)

 (RequiredState req1 ws::interaction_doro(USER_CHOOSE_MENU))
 (RequiredState req2 doro::dock(UNDOCK))
 (RequiredState req5 coro::moveto(table1))
 (RequiredState req7 doro::moveto(table1))
 (RequiredState req12 doro::dock(UNDOCK))
 (RequiredState req13 doro::moveto(user1))
 (RequiredState req15 ws::notification_doro(FOOD_DELIVERED))

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req13))
 (Constraint Duration[3000,INF](req15))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))
 (Constraint Meets(req1,req5))
 (Constraint Before(req2,req7))
 (Constraint Meets(req5,req7))
 (Constraint Meets(req7,req12))
 (Constraint Meets(req12,req13))
 (Constraint Meets(req13,req15))
)
