/*******************************************************************************
 * Copyright (c) 2010-2013 Federico Pecora <federico.pecora@oru.se>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/
package robotEraScenario;

import core.*;
import meta.configurationPlanner.RobotEraDispatchingFunction;
import meta.configurationPlanner.RobotEraSimplePlannerInferenceCallback;

import org.metacsp.dispatching.DispatchingFunction;
import org.metacsp.meta.simplePlanner.ProactivePlanningDomain;
import org.metacsp.meta.simplePlanner.SimpleDomain;
import org.metacsp.meta.simplePlanner.SimplePlanner;
import org.metacsp.multi.activity.ActivityNetworkSolver;
import org.metacsp.sensing.ConstraintNetworkAnimator;
import org.metacsp.sensing.Sensor;
import org.metacsp.time.Bounds;
import org.metacsp.utility.logging.MetaCSPLogging;
import org.metacsp.utility.timelinePlotting.TimelinePublisher;
import org.metacsp.utility.timelinePlotting.TimelineVisualizer;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FakeExecutor extends Thread {
	
	public static long TIMEOUT = 4000;

	private String[] tuplesToMonitor = null;
	private String[] tuplesToControl = null;
	private long[] tuplesToMonitorTimeouts = null;
	private int peisID = -1;
	
	public FakeExecutor(String fileName) {
		ArrayList<String> parameters = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (!line.trim().equals("")) parameters.add(line.trim());
			}
			br.close();
		}
		catch (FileNotFoundException e) { e.printStackTrace(); }
		catch (IOException e) { e.printStackTrace(); }
		
		//Init peis
		peisID = Integer.parseInt(parameters.get(0));
		String[] program_id={"--peis-id", ""+peisID};
		try { PeisJavaMT.peisjava_initialize(program_id); }
		catch (WrongPeisKernelVersionException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		tuplesToMonitor = new String[parameters.size()-1];
		tuplesToControl = new String[parameters.size()-1];
		tuplesToMonitorTimeouts = new long[parameters.size()-1];
		//Set tuples
		for (int i = 1; i < parameters.size(); i++) {
			String tupleName = parameters.get(i).split(" ")[0];
			String tupleValue = parameters.get(i).split(" ")[1];
			String actuatorTuple = tupleName.replace("out.", "in.").replace(".state",".command");
			PeisJavaMT.peisjava_setStringTuple(tupleName, tupleValue);
			PeisJavaMT.peisjava_subscribeIndirectly(actuatorTuple);
			tuplesToMonitor[i-1] = actuatorTuple;
			tuplesToControl[i-1] = tupleName;
			tuplesToMonitorTimeouts[i-1] = -1;
		}
		
		this.start();
	}

	public void run() {
		//Stayin' alive...
		while(true) {
			for (int i = 0; i < tuplesToMonitor.length; i++) {
				String tupleToMonitor = tuplesToMonitor[i];
				PeisTuple tuple = PeisJavaMT.peisjava_getTupleIndirectly(peisID, tupleToMonitor);
				if (tuple != null) {
					String tupleData = tuple.getStringData();
					if (tupleData.trim().equals("ON") && tuplesToMonitorTimeouts[i] == -1) {
						System.out.println("++++ STARTED " + tuple.getKey());
						tuplesToMonitorTimeouts[i] = Calendar.getInstance().getTimeInMillis();								
					}
					else if (tupleData.trim().equals("ON") && Calendar.getInstance().getTimeInMillis() - tuplesToMonitorTimeouts[i] > TIMEOUT) {
						tuplesToMonitorTimeouts[i] = -1;
						try { PeisJavaMT.peisjava_setStringTupleIndirectly(peisID, tupleToMonitor, "OFF"); }
						catch (PeisTupleException e) { e.printStackTrace(); }
						PeisJavaMT.peisjava_setStringTuple(tuplesToControl[i], "COMPLETED");
						System.out.println("---- ENDED " + tuple.getKey());
					}
				}
			}
			try { Thread.sleep(10); }
			catch (InterruptedException e) { e.printStackTrace(); }
		}
	}
	
	public static void main(String[] args) {
		System.out.println("ARGS ARE " + args[0]);
		new FakeExecutor(args[0]);
//		new FakeExecutor("/home/fpa/gitroot.bitbucket/robot-era-cpm/scripts/coro.fe");
	}
	

}
