package meta.configurationPlanner;

import cern.colt.Arrays;
import core.CallbackObject;
import core.PeisJavaMT;
import core.PeisTuple;

import org.metacsp.framework.ConstraintNetwork;
import org.metacsp.framework.ConstraintSolver;
import org.metacsp.framework.Variable;
import org.metacsp.framework.VariablePrototype;
import org.metacsp.framework.meta.MetaConstraint;
import org.metacsp.meta.simplePlanner.ProactivePlanningDomain;
import org.metacsp.meta.simplePlanner.SimpleOperator;
import org.metacsp.meta.simplePlanner.SimplePlanner;
import org.metacsp.meta.simplePlanner.SimpleReusableResource;
import org.metacsp.meta.simplePlanner.SimpleDomain.markings;
import org.metacsp.multi.activity.Activity;
import org.metacsp.multi.activity.SymbolicVariableActivity;
import org.metacsp.multi.allenInterval.AllenIntervalConstraint;
import org.metacsp.sensing.InferenceCallback;
import org.metacsp.sensing.Sensor;
import org.metacsp.time.APSPSolver;
import org.metacsp.time.Bounds;
import org.metacsp.utility.logging.MetaCSPLogging;
import org.metacsp.framework.Constraint;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class RobotEraSimplePlannerInferenceCallback implements InferenceCallback, Serializable {

	private static final long serialVersionUID = -6730506457770817729L;
	private SimplePlanner planner = null;
	private transient Logger logger = MetaCSPLogging.getLogger(this.getClass());
	private ProactivePlanningDomain domain = null;
	private String[] urgentRequests = null;
	private HashSet<String> servedUrgentRequests;
	
	private boolean eqmaint = false;
	private boolean doneOnce = false;
	private static final int PEIS_EQMAINT = 6666;
	private static final String TUPLE_EQMAINT = "eqmaint.dispatch.actions"; 
	private String eqmaintActions = "";

	private boolean forget = true;          // CN maintenance
	private boolean benchmark = true;       // testing the CPM
	private BufferedWriter out = null;
	
	private HashMap<Integer,String[]> goalArgumentsAndParameters = new HashMap<Integer, String[]>();
	
	public void setGoalArguments(int hashCode, String[] args) {
		goalArgumentsAndParameters.put(hashCode, args);
	}

	public RobotEraSimplePlannerInferenceCallback(SimplePlanner planner, String domainName, boolean forget) {
		this.forget = forget;
		this.planner = planner;
		
//		PeisJavaMT.peisjava_subscribe(PEIS_EQMAINT, TUPLE_EQMAINT);
//		PeisJavaMT.peisjava_registerTupleCallback(PEIS_EQMAINT, TUPLE_EQMAINT, (CallbackObject) this);
		
		MetaConstraint[] metaConstraints = planner.getMetaConstraints();
		for (MetaConstraint mc : metaConstraints) {
			if (mc instanceof ProactivePlanningDomain) {
				domain = (ProactivePlanningDomain) mc;
				String[] urgentRequestsString = domain.parseUserKeyword("UrgentRequests");
				if (urgentRequestsString.length > 0) {
					urgentRequests = urgentRequestsString[0].split("\\s+");
				}
				servedUrgentRequests = new HashSet<String>();
				break;
			}
		}
		if (benchmark) {
			Date date = new Date() ;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss") ;
			File file = new File("CPM-stats-" + domainName + "-" + dateFormat.format(date) + ".log") ;
			try { out = new BufferedWriter(new FileWriter(file)); }
			catch (IOException e) { e.printStackTrace(); }
		}
	}

	private boolean isFrozen(Activity act, String ... filter) {
		if (act.getVariable().getComponent().contains("res_capacity")) return true;
		if (!act.getVariable().getMarking().equals("Forgettable")) return false;
		Variable timeMachine = planner.getConstraintSolvers()[0].getVariables("Time")[0];
		if (act.getVariable().equals(timeMachine)) return false;
		for (String f : filter) if (act.getSymbols()[0].equals(f)) return false;
		return true;
//		Constraint[] consWithTimeMachine = planner.getConstraintSolvers()[0].getConstraints(act.getVariable(), timeMachine);
//		if (consWithTimeMachine != null && consWithTimeMachine.length > 0) return false;
//		return act.getTemporalVariable().getEST() == act.getTemporalVariable().getLST() && act.getTemporalVariable().getEET() == act.getTemporalVariable().getLET();
	}
	
	private void exploreDFS(Variable v, ConstraintNetwork cn, HashSet<Variable> vars) {
		vars.add(v);
		for (Variable nv : cn.getNeighboringVariables(v)) {
			if (!vars.contains(nv)) exploreDFS(nv, cn, vars);
		}
		
	}
	
	private String printVarNames(Variable[] vars, String ... filter) {
		String[] retA = new String[vars.length];
		for (int i = 0; i < vars.length; i++) {
			if (vars[i] instanceof Activity) {
				Activity act = (Activity)vars[i];
				retA[i] = act.getVariable().getID() + ":" + act.getVariable().getComponent() + ":" + act.getSymbols()[0];
				if (!isFrozen(act, filter)) retA[i] += "*";
			}
		}
		return Arrays.toString(retA);
	}
	
	private void getForgettableActivitiesAndConstraints(ArrayList<Variable> varsToRemove, ArrayList<Constraint> consToRemove, String ... filter) {
		ConstraintSolver cs = planner.getConstraintSolvers()[0];
		ConstraintNetwork cn = cs.getConstraintNetwork();
		ArrayList<HashSet<Variable>> connectedComponents = new ArrayList<HashSet<Variable>>();

		for (Variable v : cn.getVariables()) {
			boolean exists = false;
			for (HashSet<Variable> previousConnectedComponent : connectedComponents) {
				if (previousConnectedComponent.contains(v)) {
					exists = true;
					break;
				}
			}
			if (!exists) {
				HashSet<Variable> newConnectedComponent = new HashSet<Variable>();
				exploreDFS(v,cn,newConnectedComponent);
				connectedComponents.add(newConnectedComponent);
			}
		}
		
		logger.fine("=== " + connectedComponents.size() + " CONNECTED COMPONENT(S) ===");
		for (HashSet<Variable> connectedComponent : connectedComponents) {
			logger.fine("= " + printVarNames(connectedComponent.toArray(new Variable[connectedComponent.size()]), filter));
		}
		logger.fine("================================");
		
		for (HashSet<Variable> oneConnectedComponent : connectedComponents) {
			boolean foundNotFrozen = false;
			for (Variable v : oneConnectedComponent) {
				Activity act = (Activity)v;
				if (!isFrozen(act, filter)) {
					foundNotFrozen = true;
					break;
				}
			}
			if (!foundNotFrozen) {
				//System.out.println("------------------> ALL FROZEN: " + oneConnectedComponent);
				varsToRemove.addAll(oneConnectedComponent);
			}
		}
		
		HashSet<Constraint> consToRemoveNoRep = new HashSet<Constraint>();
		for (Variable v : varsToRemove) {
			for (Constraint c : cn.getIncidentEdges(v)) consToRemoveNoRep.add(c);
		}
		consToRemove.addAll(consToRemoveNoRep);
	}
	
	private String printConstraintSimple(Constraint con) {
		String ret = "";
		if (con instanceof AllenIntervalConstraint) {
			AllenIntervalConstraint aic = (AllenIntervalConstraint)con;
			ret += aic.getFrom().getID() + " " + aic.getTypes()[0] + " " + aic.getTo().getID();
		}
		return ret;
	}
	
	protected void finalize() throws Throwable {
		if (benchmark) out.close();
	}
	
	private Boolean dispatchEqmaintActions(long timeNow) {
//		logger.info("**************************** Eqmaint: jasmin(): doneOnce: " + doneOnce);
		
		if (doneOnce) { return true;	}
		doneOnce = true;
		
		if (eqmaintActions.isEmpty()) {	return true; }
		
//		String[] operators = new String("(!req_init shirt);(!req_a1 coro moveto door1);(!req_a2 ws1 notification_doro coro_at_entrance);"
//				+ "(!req_a2a ws1 interaction_doro system_service_request);(!req7 coro moveto outside_elevator_floor2);"
//				+ "(!req_d1 coro moveto door1);(!req_a2 ws1 notification_doro coro_at_entrance);"
//				+ "(!req_a2a ws1 interaction_doro system_service_request);(!req_final shirt)").split(";");
		
//	 (RequiredState req1 doro::moveto(user1))
//	 (RequiredState req3 doro::dock(USER1))
//	 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
//		String[] operators = new String("(!req1 doro moveto user1);(!req3 doro dock USER1);(!req4 ws interaction_doro USER_SERVICE_REQUEST)").split(";");
		// (!req1 doro dock undock);(!req2 doro moveto doro_home);(!req100 doro stop)
		// (!req1 doro moveto user1);(!req3 doro dock USER1);(!req4 ws interaction_doro USER_SERVICE_REQUEST);(!req100 doro stop)

		String[] operators = eqmaintActions.split(";");
		
		ArrayList<SymbolicVariableActivity> acts = new ArrayList<SymbolicVariableActivity>();
		ArrayList<Constraint> consToAdd = new ArrayList<Constraint>();
		
		for (String op : operators) {
//			logger.info("**************************** Eqmaint: operator: " + op);
			String[] elements = op.split(" ");
			if (elements.length > 2) {
				String component = elements[1].replace("(", "").replace(")", "");
				String action = elements[2].replace("(", "").replace(")", "");
				String par = "";
				if (elements.length > 3) par = elements[3].replace("(", "").replace(")", "");
				logger.info("**************************** Eqmaint: component: " + component + ", action: " + action + ", par: " + par);
				Variable var = planner.getConstraintSolvers()[0].createVariable(component);
				SymbolicVariableActivity a  = (SymbolicVariableActivity)var;
				a.setSymbolicDomain(action + "(" + par + ")");
				a.setMarking(markings.JUSTIFIED);
				acts.add(a);
				AllenIntervalConstraint duration = new AllenIntervalConstraint(AllenIntervalConstraint.Type.Duration, new Bounds(3500,APSPSolver.INF));
				duration.setFrom(a);
				duration.setTo(a);
				consToAdd.add(duration);
			}
		}


		AllenIntervalConstraint release = new AllenIntervalConstraint(AllenIntervalConstraint.Type.Release, new Bounds(timeNow,APSPSolver.INF));
		release.setFrom(acts.get(0));
		release.setTo(acts.get(0));
		consToAdd.add(release);
		
		for (int i = 0; i < acts.size()-1; i++) {
			Variable from = acts.get(i);
			Variable to = acts.get(i+1);
			AllenIntervalConstraint before = new AllenIntervalConstraint(AllenIntervalConstraint.Type.Before);
			before.setFrom(from);
			before.setTo(to);
			consToAdd.add(before);
		}
		
		planner.getConstraintSolvers()[0].addConstraints(consToAdd.toArray(new Constraint[consToAdd.size()]));
		
		return true;
	}
	
	@Override
	public void doInference(long timeNow) {
		if (planner != null) {
			long inferenceTime = Calendar.getInstance().getTimeInMillis();
			domain.resetContextInference();
			domain.updateTimeNow(timeNow);
			planner.clearResolvers();
			final int CPM_ID = 995;
			String sensor = "in.goal.type";
			PeisTuple ret = null;
			while ((ret = PeisJavaMT.peisjava_getTupleIndirectly(CPM_ID, sensor)) == null) {
				try { Thread.sleep(10); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
			String currentGoal = "null";
			String[] values_in_type_tuple = null;

			if (ret != null) {
                // a new goal is posted
				currentGoal = ret.getStringData().split(" ")[0];
			}
						
//			String[] values_in_type_tuple = currentGoal.split(" ");		
//			currentGoal = values_in_type_tuple[0];
			//System.out.println("currentGoal is now: " + currentGoal);

			//FORGET IF POSSIBLE
			if (forget) {
				ArrayList<Variable> varsToRemove = new ArrayList<Variable>();
				ArrayList<Constraint> consToRemove = new ArrayList<Constraint>();
				getForgettableActivitiesAndConstraints(varsToRemove, consToRemove, currentGoal);
				logger.info("Forgetting " + varsToRemove.size() + " variables and " + consToRemove.size() + " constraints");
				HashMap<SimpleReusableResource,ArrayList<Variable>> usagesToRemove = new HashMap<SimpleReusableResource,ArrayList<Variable>>();
				for (Variable var : varsToRemove) {
					for (Entry<String,SimpleReusableResource> e : domain.getResources().entrySet()) {
//						System.out.println("RESOURCE: " + e.getKey() + " CAP: " + e.getValue().getCapacity());
						for (Activity v : e.getValue().getActivityOnUse()) {
							if (v.getVariable().equals(var)) {
								if (!usagesToRemove.containsKey(e.getValue())) {
									usagesToRemove.put(e.getValue(),new ArrayList<Variable>());
								}
								usagesToRemove.get(e.getValue()).add(var);
							}
						}
					}
				}
				for (Entry<SimpleReusableResource,ArrayList<Variable>> e : usagesToRemove.entrySet()) {
					for (Variable v : e.getValue()) {
						e.getKey().removeUsage((Activity)v);
					}
					logger.fine("Removed usage of " + printVarNames(e.getValue().toArray(new Variable[e.getValue().size()])));
				}
				planner.getConstraintSolvers()[0].removeConstraints(consToRemove.toArray(new Constraint[consToRemove.size()]));
				planner.getConstraintSolvers()[0].removeVariables(varsToRemove.toArray(new Variable[varsToRemove.size()]));
			}

			boolean inNetwork = false;
			boolean isAnUrgentRequest = false;
			String currentUrgentRequest = "null";
			if ( !currentGoal.equals("null") ) {
				Variable[] sensedGoals = planner.getConstraintSolvers()[0].getVariables(sensor);
				for (Variable v : sensedGoals) {
					Activity act = (Activity) v;
					if (act.getSymbols()[0].equals(currentGoal)) {
						if (act.getTemporalVariable().getEET() >= timeNow) {
							//sensor value is already in network
							inNetwork = true;
							values_in_type_tuple = goalArgumentsAndParameters.get(Sensor.getHashCode(sensor, currentGoal, act.getTemporalVariable().getEST()));
							//logger.info("Retrieved parameters and arguments for " + currentGoal + ": " + Arrays.toString(values_in_type_tuple));
							if (null != urgentRequests) {
								for(String urgentRequest:urgentRequests) {
									if ( currentGoal.equals(urgentRequest) ) {
										isAnUrgentRequest = true;
										currentUrgentRequest = v.getComponent() + ((Activity) v).getSymbols()[0] + v.getID();
									}
								}
							}

                            if ( currentGoal.equals("REMIND_EVENT") ) {
                                // Adding a temporal constraint to the planning domain model
                                for(SimpleOperator operator: domain.getOperators()) {
                                    if ( operator.getHead().contains("REMIND_EVENT") ) {
                                        int nbRequirementActivities = operator.getRequirementActivities().length;
                                        int indexDoroWait = -1;
                                        for (int i = 0 ; i < nbRequirementActivities ; i++) {
                                            if (operator.getRequirementActivities()[i].equals("doro::wait(in.goal.parameters)")) {
                                                indexDoroWait = i;
                                                break;
                                            }
                                        }
                                        // adding constraint Duration
                                        long remind_time;
					                    remind_time = 120 * 1000; // 2 minutes
                                        values_in_type_tuple[1] = "120";
                                        // if(values_in_type_tuple[1].equals("NONE")) {
                                        // 	remind_time = 120 * 1000;
                                        // }
                                        // else {
                                        // 	remind_time = Integer.parseInt(values_in_type_tuple[1]) * 1000; // parameters
                                        // }
                                        operator.getExtraConstraints()[indexDoroWait + 1][indexDoroWait + 1] =
                                                new AllenIntervalConstraint(AllenIntervalConstraint.Type.Duration,
                                                        new Bounds(remind_time,remind_time));
                                        break;
                                    }
                                }
                            } else if ( currentGoal.equals("BRING") ) {
                                // Replacing string TAKE_SOMETHING by either TAKE_MEDICINE or TAKE_WATER, depending on
                                // the value of in.goal.args
                                String newAction = "";
                                if ( values_in_type_tuple[0].equals("pills") ) {
                                    newAction = "ws::notification_doro(TAKE_PILLS)";
                                } else if ( values_in_type_tuple[0].equals("water") ) {
                                    newAction = "ws::notification_doro(TAKE_WATER)";
                                }
                                for(SimpleOperator operator: domain.getOperators()) {
                                    if ( operator.getHead().contains("BRING") ) {
                                        int nbRequirementActivities = operator.getRequirementActivities().length;
                                        for (int i = 0 ; i < nbRequirementActivities ; i++) {
                                            if (operator.getRequirementActivities()[i].equals("ws::notification_doro(TAKE_SOMETHING)")) {
                                                operator.getRequirementActivities()[i] = newAction;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

						}
					}
				}
			}

			if ( isAnUrgentRequest && (!servedUrgentRequests.contains(currentUrgentRequest)) ) {
				servedUrgentRequests.add(currentUrgentRequest);
				//                System.out.println(currentUrgentRequest);
				Variable[] cnVariables = planner.getConstraintSolvers()[0].getVariables();
				Vector<Variable> variablesToBeRemoved = new Vector<Variable>();
				for (Variable v:cnVariables) {
					Activity act = (Activity) v;
					if ( act.getTemporalVariable().getEST() > timeNow ) {
						//                          System.out.println("Variable to be removed: " + act.getSymbols()[0]);
						variablesToBeRemoved.add(v);
					}
				}

				HashSet<Constraint> constraintsToBeRemoved = new HashSet<Constraint>();

				for (Variable v : variablesToBeRemoved) {
					//Activity act = (Activity) v;
					for (Constraint con : planner.getConstraintSolvers()[0].getConstraintNetwork().getIncidentEdges(v)) {
						boolean removed = constraintsToBeRemoved.add(con);
						//                        if (removed) System.out.println("Constraint to be removed: " + con);
					}
				}

				planner.getConstraintSolvers()[0].removeConstraints( constraintsToBeRemoved.
						toArray(new Constraint[constraintsToBeRemoved.size()]) );

				planner.getConstraintSolvers()[0].removeVariables( variablesToBeRemoved.
						toArray(new Variable[variablesToBeRemoved.size()]) );
			}

			Boolean planningSucceeds = false;			
			if (!eqmaint) { 
//				logger.info("**************************** NO Opp.reasoning");
				planningSucceeds = planner.backtrack();	
			}
			else {
//				logger.info("**************************** Doing Opp.reasoning...");
//				PeisJavaMT.peisjava_subscribe(PEIS_EQMAINT, TUPLE_EQMAINT);
//				PeisJavaMT.peisjava_registerTupleCallback(PEIS_EQMAINT, TUPLE_EQMAINT, (CallbackObject) this);
				planningSucceeds = dispatchEqmaintActions(timeNow); 
			}

			if (isAnUrgentRequest && planningSucceeds) {
				Variable[] sensedGoals = planner.getConstraintSolvers()[0].getVariables(sensor);

				for (Variable v:sensedGoals) {
					String urgentRequestUnification = v.getComponent() + ((Activity) v).getSymbols()[0] + v.getID();
					servedUrgentRequests.add(urgentRequestUnification);
				}

			}
						
			if (!currentGoal.equals("null") && inNetwork) {
				String currentStatus = PeisJavaMT.peisjava_getStringTuple("out.goal.state");
				if (null == currentStatus) {
					currentStatus = "IDLE";
				}
                if ( currentGoal.trim().equals("NULL") && !currentStatus.trim().equals("IDLE") ) {
                    PeisJavaMT.peisjava_setStringTuple("out.goal.state", "IDLE");
                }
                if ( currentStatus.trim().equals("IDLE") && !currentGoal.trim().equals("NULL") ) {
					if (planningSucceeds) {
												
						logger.info("Planning COMPLETED with success.");

						String arguments = "nulle";
						String parameters = "nulle";
						
						//sensor = "in.goal.args";
						if(values_in_type_tuple.length < 2) {
							logger.info("FATAL: WE HAVE LESS THAN 2 PARAMETERS!!! RESTART THE PLANNER.");
						}
						else {
							arguments = values_in_type_tuple[0];
							parameters = values_in_type_tuple[1];
						}
						
						logger.info("DEREFERENCING in.goal.args WITH " + arguments);
						logger.info("DEREFERENCING in.goal.parameters WITH " + parameters);

						Variable[] solutionVariables = planner.getConstraintSolvers()[0].getVariables();

						//                        System.out.println("Iterating over solution plan variables.");
						for (Variable variable:solutionVariables) {
							if (variable instanceof SymbolicVariableActivity) {
								SymbolicVariableActivity activity = (SymbolicVariableActivity)variable;
								//                                System.out.println("Inspecting an activity.");
								if ( domain.isActuator(activity.getComponent()) ) {
									//                                    System.out.println("Inspecting an actuator.");

									String activitySymbol = activity.getSymbolicVariable().getSymbols()[0];
									activitySymbol = activitySymbol.replace("in.goal.args",arguments);
									activitySymbol = activitySymbol.replace("in.goal.parameters",parameters);
									activity.getSymbolicVariable().setDomain(activitySymbol);

								}


							}
						}

					} else {
						PeisJavaMT.peisjava_setStringTuple("out.goal.state", "FAILED");
						logger.info("Planning FAILED!");
					}

					PeisJavaMT.peisjava_setStringTuple("out.goal.state", "COMPLETED");
					logger.info("*******************************************************");

				}
				else {
//					logger.info("DID NOT ENTER THE LOOP!!!");
//					logger.info("Current State is: " + currentStatus);
//					logger.info("Current Goal is: " + currentGoal);
				}
                if ( currentGoal.equals("REMIND_EVENT") ) {
                    // Removing a temporal constraint from the planning domain model
                    for(SimpleOperator operator: domain.getOperators()) {
                        if (operator.getHead().contains("REMIND_EVENT")) {
                            int nbRequirementActivities = operator.getRequirementActivities().length;
                            int indexDoroWait = -1;
                            for (int i = 0 ; i < nbRequirementActivities ; i++) {
                                if (operator.getRequirementActivities()[i].equals("doro::wait(in.goal.parameters)")) {
                                    indexDoroWait = i;
                                    break;
                                }
                            }
                            // removing constraint Duration
                            operator.getExtraConstraints()[indexDoroWait + 1][indexDoroWait + 1] = null;
                            break;

                        }
                    }
                } else if ( currentGoal.equals("BRING") ) {
                    // Replacing string TAKE_XXX by TAKE_SOMETHING, where XXX stands for either MEDICINE or WATER
                    for(SimpleOperator operator: domain.getOperators()) {
                        if (operator.getHead().contains("BRING")) {
                            int nbRequirementActivities = operator.getRequirementActivities().length;
                            for (int i = 0 ; i < nbRequirementActivities ; i++) {
                                if (operator.getRequirementActivities()[i].contains("ws::notification_doro(TAKE_")) {
                                    operator.getRequirementActivities()[i] = "ws::notification_doro(TAKE_SOMETHING)";
                                    break;
                                }
                            }
                            break;
                        }
                    }

                }
			}


			for (ConstraintNetwork cn : planner.getAddedResolvers()) {
				VariablePrototype var = null;
				for (Variable v : cn.getVariables()) {
					if (v instanceof VariablePrototype) {
						if (((VariablePrototype)v).getParameters().length > 2) {
							if (((VariablePrototype)v).getParameters()[2].equals("Inference")) {
								var = (VariablePrototype)v;
							}
						}
					}
				}
				if (var != null) {
					SymbolicVariableActivity act = (SymbolicVariableActivity)cn.getSubstitution(var);
					domain.setOldInference(act.getComponent(), act);
				}
			}
			
			if (benchmark) {
				try {
					long currentTime = Calendar.getInstance().getTimeInMillis();
					long elapsed = currentTime - inferenceTime;
					int numVars = planner.getConstraintSolvers()[0].getVariables().length;
					int numCons = planner.getConstraintSolvers()[0].getConstraints().length;
                    // elapsed is the computation time for a planning cycle
					out.write(numVars + "\t" + numCons + "\t" + elapsed + "\n");
					out.flush();
				}
				catch (IOException e) { e.printStackTrace(); }
			}
		}
	}



}
