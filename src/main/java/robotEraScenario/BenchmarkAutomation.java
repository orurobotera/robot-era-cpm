/*******************************************************************************
 * Copyright (c) 2010-2013 Federico Pecora <federico.pecora@oru.se>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/
package robotEraScenario;

import core.*;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class BenchmarkAutomation extends Thread {
	
	public static int MIN_DELAY = 20000;
	public static int MAX_DELAY = 40000;
	private String[] tasks = null;
	private static int peisID = 8888;
	
	public BenchmarkAutomation(String fileName) {
		ArrayList<String> parameters = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (!line.trim().equals("") && !line.trim().startsWith("#")) parameters.add(line.trim());
			}
			br.close();
		}
		catch (FileNotFoundException e) { e.printStackTrace(); }
		catch (IOException e) { e.printStackTrace(); }
		
		tasks = parameters.toArray(new String[parameters.size()]);
		
		//Init peis
		String[] program_id={"--peis-id", ""+peisID};
		try { PeisJavaMT.peisjava_initialize(program_id); }
		catch (WrongPeisKernelVersionException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		this.start();
	}

	public void run() {
		//Stayin' alive...
		Random rand = new Random(12123);
		int sleep = 0;
		long postingTime = 0;
        String previousTask = "";
		while(true) {
			if (Calendar.getInstance().getTimeInMillis()-postingTime > sleep) {
				postingTime = Calendar.getInstance().getTimeInMillis();
				sleep = rand.nextInt(MAX_DELAY-MIN_DELAY)+MIN_DELAY;
                String task;
                do {
                    task = tasks[rand.nextInt(tasks.length)];
                } while (task.equals(previousTask));
                previousTask = task;
				PeisJavaMT.peisjava_setRemoteStringTuple(TestScenario.CAM_ID, "out.goal.type", task);
				System.out.println("Posted goal " + task + " / Waiting for "+  sleep + " milliseconds");
				try { Thread.sleep(10); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
		}
	}
	
	public static void main(String[] args) {
		new BenchmarkAutomation(args[0]);
	}
	

}
