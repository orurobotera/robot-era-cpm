##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain GarbageCollection)


#(TimelinesToShow Time coro doro elevator ws dustcart user1.pos.geo home.pos.gps discharge.pos.gps in.goal.type in.goal.args inference.goal.type)


########################
# sensor tuples on CAM #
########################

(Sensor user1.pos.geo)
(Sensor in.goal.type)
# CASE_B with only Coro transporting the garbage bag
(Sensor in.goal.args)

# Locations used by Dustcart.
(Sensor dustcart_home.pos.gps)
(Sensor discharge.pos.gps)

# desired states (goals) should be inferred for doro
(ContextVariable inference.goal.type)


#############
# Actuators #
#############

# robots' states cannot occur in the past (they are actions to be executed)
(Actuator coro)
(Actuator ws)
(Actuator doro)
(Actuator elevator)
(Actuator dustcart)


######################
# Planning operators #
######################

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)

# operators used for inferring desired goals and planning
(SimpleOperator
 (Head inference.goal.type::GARBAGE_COLLECTION)

 (RequiredState req_a0 in.goal.type::GARBAGE_COLLECTION)

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req_c6b elevator::door(CLOSE))
 (RequiredState req_c6 elevator::moveto(FLOOR0))
 (RequiredState req_c7 elevator::door(OPEN))
 (RequiredState req_c6a coro::moveto(elevator_floor0))
 (RequiredState req_c9 elevator::door(CLOSE))
 (RequiredState req_c10 elevator::moveto(FLOOR1))
 (RequiredState req_c11 elevator::door(OPEN))
 (RequiredState req_c11a coro::loadmap(FLOOR1))
 (RequiredState req_c12 coro::moveto(outside_elevator_floor1))

# Below this is - coro going to the Apartment
 (RequiredState req_a1 coro::moveto(door1))
 (RequiredState req_a1b elevator::door(CLOSE))
# (RequiredState req_a2 ws::notification_doro(CORO_AT_ENTRANCE))
# (RequiredState req_a3 ws::interaction_coro(GARBAGE_PICK_UP))

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req6 elevator::moveto(FLOOR1))
 (RequiredState req7 elevator::door(OPEN))
 (RequiredState req6a coro::moveto(elevator_floor1))
 (RequiredState req9 elevator::door(CLOSE))
 (RequiredState req10 elevator::moveto(FLOOR0))
 (RequiredState req11 elevator::door(OPEN))
 (RequiredState req11a coro::loadmap(FLOOR0))
 (RequiredState req12 coro::moveto(outside_elevator_floor0))
 (RequiredState req13 elevator::door(CLOSE))

 # EVERYTHING BELOW IS THE GOODS-EXCHANGE, CORO--DUSTCART.
 (RequiredState req coro::moveto(meetingpoint))
 (RequiredState req0a dustcart::moveto(dustcart_home.pos.gps))
 (RequiredState req0 dustcart::dock(DOCK))
 (RequiredState req1 coro::miradock(DUSTCART))
 (RequiredState req2 coro::roller(UNLOAD))
 (RequiredState req3 dustcart::bin(LOAD))
 (RequiredState req4 coro::miradock(UNDOCK))
 (RequiredState req5 dustcart::dock(UNDOCK))

 (RequiredState req_b0 coro::moveto(coro_home))
 (RequiredState req_b1 dustcart::moveto(discharge.pos.gps))
 (RequiredState req_b2 dustcart::garbage(DISCHARGE))
 (RequiredState req_b3 dustcart::moveto(dustcart_home.pos.gps))


 (Constraint Duration[3000,INF](req_c6b))
 (Constraint Duration[3000,INF](req_c6))
 (Constraint Duration[3000,INF](req_c7))
 (Constraint Duration[3000,INF](req_c6a))
 (Constraint Duration[3000,INF](req_c9))
 (Constraint Duration[3000,INF](req_c10))
 (Constraint Duration[3000,INF](req_c11))
 (Constraint Duration[3000,INF](req_c11a))
 (Constraint Duration[3000,INF](req_c12))

 (Constraint Duration[3000,INF](req_a1))
 (Constraint Duration[3000,INF](req_a1b))
# (Constraint Duration[3000,INF](req_a2))
# (Constraint Duration[3000,INF](req_a3))

 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req6a))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req11a))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req13))

 (Constraint Duration[3000,INF](req))
 (Constraint Duration[3000,INF](req0a))
 (Constraint Duration[3000,INF](req0))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))

 (Constraint Duration[3000,INF](req_b0))
 (Constraint Duration[3000,INF](req_b1))
 (Constraint Duration[3000,INF](req_b2))
 (Constraint Duration[3000,INF](req_b3))


 (Constraint Starts[10,10](Head,req_a0))

 (Constraint Meets(req_c6b,req_c6))
 (Constraint Meets(req_c6,req_c7))
 (Constraint Before(req_c7,req_c6a))
 (Constraint Before(req_c6a,req_c9))
# (Constraint Before(req_c7,req_c9))
 (Constraint Meets(req_c9,req_c10))
 (Constraint Meets(req_c10,req_c11))
 (Constraint Meets(req_c9,req_c11a))
 (Constraint Before(req_c11a,req_c12))
 (Constraint Before(req_c11,req_c12))

 (Constraint Meets(req_c12,req_a1))
 (Constraint Meets(req_c12,req_a1b))
# (Constraint Meets(req_a1,req_a2))
# (Constraint Meets(req_a2,req_a3))

# (Constraint Before(req_a3,req6a))
 (Constraint Before(req_a1b,req6))

 (Constraint Meets(req6,req7))
 (Constraint Before(req7,req6a))
 (Constraint Before(req6a,req9))
# (Constraint Before(req7,req9))
 (Constraint Meets(req9,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req9,req11a))
 (Constraint Before(req11a,req12))
 (Constraint Before(req11,req12))
 (Constraint Meets(req12,req13))

 (Constraint Meets(req12,req))
 (Constraint Before(req12,req0a))

 (Constraint Before(req,req0))
 (Constraint Before(req0a,req0))
 (Constraint Meets(req0,req1))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req2,req4))
 (Constraint Before(req4,req5))

 (Constraint Meets(req4,req_b0))
 (Constraint Meets(req5,req_b1))

 (Constraint Meets(req_b1,req_b2))
 (Constraint Meets(req_b2,req_b3))

)
