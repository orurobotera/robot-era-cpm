set xlabel "Iteration"

#set ylabel "# Variables"
#plot 'CPM-stats-ScenarioOne-NO_OPT-2015-06-30-19-10-20.log' using 1 with l title "NO-OPT", 'CPM-stats-ScenarioOne-OPT-2015-06-30-19-22-55.log' using 1 with l title "OPT"

#set ylabel "# Constraints"
#plot 'CPM-stats-ScenarioOne-NO_OPT-2015-06-30-19-10-20.log' using 2 with l title "NO-OPT", 'CPM-stats-ScenarioOne-OPT-2015-06-30-19-22-55.log' using 2 with l title "OPT"

set ylabel "# Solving time [msec]"
plot 'CPM-stats-ScenarioOne-NO_OPT-2015-06-30-19-10-20.log' using 3 with l title "NO-OPT", 'CPM-stats-ScenarioOne-OPT-2015-06-30-19-22-55.log' using 3 with l title "OPT"
