#!/bin/sh
if [ $# -eq 0 ]; then
    goal=""
else
    goal=$1
fi
peismaster --peis-id 9898 --peis-set-tuple user1.pos.geo "0,1,2,3,4,5,6" --peis-set-tuple out.goal.type "$goal"
