##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain IndoorWalking)

(TimelinesToShow Time doro ws user1.pos.geo in.goal.type in.goal.args inference.goal.type)



########################
# sensor tuples on CAM #
########################

# The type of goal. Eg: ESCORT_USER
(Sensor in.goal.type)
(Sensor in.goal.args)

# Position of the user
(Sensor user1.pos.geo)

##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)


#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)

######################
# Planning operators #
######################

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)

# doro escorts the user who holds it
(SimpleOperator
 (Head inference.goal.type::ESCORT_USER)
 (RequiredState req0 in.goal.type::ESCORT_USER)
 (RequiredState req1 doro::handle(ENABLE))
 (Constraint Duration[3000,INF](req1))
 (Constraint Starts[10,10](Head,req0))
)
