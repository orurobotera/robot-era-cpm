##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain LaundryTransport)





########################
# sensor tuples on CAM #
########################

(Sensor user1.pos.geo)
# The type of goal. Eg: LAUNDRY_LOAD
(Sensor in.goal.type)
(Sensor in.goal.args)


##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)



#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be executed)
(Actuator coro)
(Actuator elevator)
(Actuator ws)
(Actuator doro)


######################
# Planning operators #
######################


# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)




# Transport laundry, wash it, and go back to user with clean clothes
(SimpleOperator
 (Head inference.goal.type::LAUNDRY_LOAD)
 (RequiredState req0 in.goal.type::LAUNDRY_LOAD)

# TRAVEL TO USER, LOAD LAUNDRY
 (RequiredState req_a1 coro::moveto(door1))
 (RequiredState req_a2 ws::notification_doro(CORO_AT_ENTRANCE))
 (RequiredState req_a3 ws::interaction_coro(HI_Condominium))

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req6 elevator::moveto(FLOOR1))
 (RequiredState req7 elevator::door(OPEN))
 (RequiredState req6a coro::moveto(elevator_floor1))
 (RequiredState req9 elevator::door(CLOSE))
 (RequiredState req10 elevator::moveto(FLOOR6))
 (RequiredState req11 elevator::door(OPEN))
 (RequiredState req11a coro::moveto(outside_elevator_floor6))
 (RequiredState req12 coro::loadmap(FLOOR6))
 (RequiredState req13 elevator::door(CLOSE))

# Below this is - coro going to the laundry facility
 (RequiredState req_b0 coro::moveto(launderette))

 (RequiredState req_b1 coro::wait(LAUNDRY))

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req_c6 elevator::moveto(FLOOR6))
 (RequiredState req_c7 elevator::door(OPEN))
 (RequiredState req_c6a coro::moveto(elevator_floor6))
 (RequiredState req_c9 elevator::door(CLOSE))
 (RequiredState req_c10 elevator::moveto(FLOOR1))
 (RequiredState req_c11 elevator::door(OPEN))
 (RequiredState req_c11a coro::moveto(outside_elevator_floor1))
 (RequiredState req_c12 coro::loadmap(FLOOR1))
 (RequiredState req_c13 elevator::door(CLOSE))

# TRAVEL BACK TO USER, UNLOAD LAUNDRY
 (RequiredState req_d1 coro::moveto(door1))
 (RequiredState req_d2 ws::notification_doro(CORO_AT_ENTRANCE))
 (RequiredState req_d3 ws::interaction_coro(HI_Condominium))
 
 (Constraint Duration[300,INF](Head))

 (Constraint Duration[3000,INF](req_a1))
 (Constraint Duration[3000,INF](req_a2))
 (Constraint Duration[3000,INF](req_a3))

 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req6a))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req11a))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req13))

 (Constraint Duration[3000,INF](req_b0))
 (Constraint Duration[3000,INF](req_b1))

 (Constraint Duration[3000,INF](req_c6))
 (Constraint Duration[3000,INF](req_c7))
 (Constraint Duration[3000,INF](req_c6a))
 (Constraint Duration[3000,INF](req_c9))
 (Constraint Duration[3000,INF](req_c10))
 (Constraint Duration[3000,INF](req_c11))
 (Constraint Duration[3000,INF](req_c11a))
 (Constraint Duration[3000,INF](req_c12))
 (Constraint Duration[3000,INF](req_c13))

 (Constraint Duration[3000,INF](req_d1))
 (Constraint Duration[3000,INF](req_d2))
 (Constraint Duration[3000,INF](req_d3))


 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req_a1,req_a2))
 (Constraint Meets(req_a2,req_a3))

 (Constraint Meets(req_a3,req6a))

 (Constraint Meets(req6,req7))
 (Constraint Meets(req6a,req9))
 (Constraint Before(req7,req9))
 (Constraint Meets(req9,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req11,req11a))
 (Constraint Meets(req11a,req12))
 (Constraint Meets(req11a,req13))

 (Constraint Before(req13,req_c6))

 (Constraint Meets(req12,req_b0))

 (Constraint Meets(req_b0,req_b1))

 (Constraint Meets(req_b1,req_c6a))

 (Constraint Meets(req_c6,req_c7))
 (Constraint Meets(req_c6a,req_c9))
 (Constraint Before(req_c7,req_c9))
 (Constraint Meets(req_c9,req_c10))
 (Constraint Meets(req_c10,req_c11))
 (Constraint Meets(req_c11,req_c11a))
 (Constraint Meets(req_c11a,req_c12))
 (Constraint Meets(req_c11a,req_c13))

 (Constraint Meets(req_c12,req_d1))

 (Constraint Meets(req_d1,req_d2))
 (Constraint Meets(req_d2,req_d3))

)


