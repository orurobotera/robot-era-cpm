##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain ScenarioTwo)


# (TimelinesToShow Time doro ws inference.goal.type in.goal.type in.goal.args in.goal.parameters doro_home.pos.gps)
# (TimelinesToShow Time doro ws doro_home.pos.gps in.goal.type in.goal.args inference.goal.type)




########################
# sensor tuples on CAM #
########################

# The type of goal.
# REACH_USER_REQUEST, DELIVERY_SERVICE, GARBAGE_COLLECTION, OUTDOOR_WALKING, NO_REQUEST
(Sensor in.goal.type)


##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)


#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)
(Actuator coro)
(Actuator elevator)
(Actuator dustcart)


###################
# Unary resources #
###################

(Resource doro_resource 1)
(Resource ws_resource 1)
(Resource coro_resource 1)
(Resource elevator_resource 1)
(Resource dustcart_resource 1)


######################
# Planning operators #
######################

# operators used for inferring desired goals and planning

# User wants to walk outside with the help of dustcart
(SimpleOperator
 (Head inference.goal.type::OUTDOOR_WALKING)

 (RequiredState req0 in.goal.type::OUTDOOR_WALKING)

 (RequiredState req1 dustcart::moveto(home.pos.gps))
 (RequiredState req2 dustcart::startgui(WALKSUPPORT))

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))

 (RequiredState req_res_dustcart dustcart_res_capacity::used)
 (Constraint During(req1,req_res_dustcart))
 (Constraint During(req2,req_res_dustcart))


)


(SimpleOperator
 (Head inference.goal.type::GARBAGE_COLLECTION)

 (RequiredState req_a0 in.goal.type::GARBAGE_COLLECTION)

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req_c6b elevator::door(CLOSE))
 (RequiredState req_c6 elevator::moveto(FLOOR0))
 (RequiredState req_c7 elevator::door(OPEN))
 (RequiredState req_c6a coro::moveto(elevator_floor0))
 (RequiredState req_c9 elevator::door(CLOSE))
 (RequiredState req_c10 elevator::moveto(FLOOR1))
 (RequiredState req_c11 elevator::door(OPEN))
 (RequiredState req_c11a coro::loadmap(FLOOR1))
 (RequiredState req_c12 coro::moveto(outside_elevator_floor1))

# Below this is - coro going to the Apartment
 (RequiredState req_a1 coro::moveto(door1))
 (RequiredState req_a1b elevator::door(CLOSE))
# (RequiredState req_a2 ws::notification_doro(CORO_AT_ENTRANCE))
# (RequiredState req_a3 ws::interaction_coro(GARBAGE_PICK_UP))

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req6 elevator::moveto(FLOOR1))
 (RequiredState req7 elevator::door(OPEN))
 (RequiredState req6a coro::moveto(elevator_floor1))
 (RequiredState req9 elevator::door(CLOSE))
 (RequiredState req10 elevator::moveto(FLOOR0))
 (RequiredState req11 elevator::door(OPEN))
 (RequiredState req11a coro::loadmap(FLOOR0))
 (RequiredState req12 coro::moveto(outside_elevator_floor0))
 (RequiredState req13 elevator::door(CLOSE))

 # EVERYTHING BELOW IS THE GOODS-EXCHANGE, CORO--DUSTCART.
 (RequiredState req coro::moveto(meetingpoint))
 (RequiredState req0a dustcart::moveto(dustcart_home.pos.gps))
 (RequiredState req0 dustcart::dock(DOCK))
 (RequiredState req1 coro::miradock(DUSTCART))
 (RequiredState req2 coro::roller(UNLOAD))
 (RequiredState req3 dustcart::bin(LOAD))
 (RequiredState req4 coro::miradock(UNDOCK))
 (RequiredState req5 dustcart::dock(UNDOCK))

 (RequiredState req_b0 coro::moveto(coro_home))
 (RequiredState req_b1 dustcart::moveto(discharge.pos.gps))
 (RequiredState req_b2 dustcart::garbage(DISCHARGE))
 (RequiredState req_b3 dustcart::moveto(dustcart_home.pos.gps))


 (Constraint Duration[3000,INF](req_c6b))
 (Constraint Duration[3000,INF](req_c6))
 (Constraint Duration[3000,INF](req_c7))
 (Constraint Duration[3000,INF](req_c6a))
 (Constraint Duration[3000,INF](req_c9))
 (Constraint Duration[3000,INF](req_c10))
 (Constraint Duration[3000,INF](req_c11))
 (Constraint Duration[3000,INF](req_c11a))
 (Constraint Duration[3000,INF](req_c12))

 (Constraint Duration[3000,INF](req_a1))
 (Constraint Duration[3000,INF](req_a1b))
# (Constraint Duration[3000,INF](req_a2))
# (Constraint Duration[3000,INF](req_a3))

 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req6a))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req11a))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req13))

 (Constraint Duration[3000,INF](req))
 (Constraint Duration[3000,INF](req0a))
 (Constraint Duration[3000,INF](req0))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))

 (Constraint Duration[3000,INF](req_b0))
 (Constraint Duration[3000,INF](req_b1))
 (Constraint Duration[3000,INF](req_b2))
 (Constraint Duration[3000,INF](req_b3))


 (Constraint Starts[10,10](Head,req_a0))

 (Constraint Meets(req_c6b,req_c6))
 (Constraint Meets(req_c6,req_c7))
 (Constraint Before(req_c7,req_c6a))
 (Constraint Before(req_c6a,req_c9))
# (Constraint Before(req_c7,req_c9))
 (Constraint Meets(req_c9,req_c10))
 (Constraint Meets(req_c10,req_c11))
 (Constraint Meets(req_c9,req_c11a))
 (Constraint Before(req_c11a,req_c12))
 (Constraint Before(req_c11,req_c12))

 (Constraint Meets(req_c12,req_a1))
 (Constraint Meets(req_c12,req_a1b))
# (Constraint Meets(req_a1,req_a2))
# (Constraint Meets(req_a2,req_a3))

# (Constraint Before(req_a3,req6a))
 (Constraint Before(req_a1b,req6))

 (Constraint Meets(req6,req7))
 (Constraint Before(req7,req6a))
 (Constraint Before(req6a,req9))
# (Constraint Before(req7,req9))
 (Constraint Meets(req9,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req9,req11a))
 (Constraint Before(req11a,req12))
 (Constraint Before(req11,req12))
 (Constraint Meets(req12,req13))

 (Constraint Meets(req12,req))
 (Constraint Before(req12,req0a))

 (Constraint Before(req,req0))
 (Constraint Before(req0a,req0))
 (Constraint Meets(req0,req1))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req2,req4))
 (Constraint Before(req4,req5))

 (Constraint Meets(req4,req_b0))
 (Constraint Meets(req5,req_b1))

 (Constraint Meets(req_b1,req_b2))
 (Constraint Meets(req_b2,req_b3))

 (RequiredState req_res_coro coro_res_capacity::used)
 (RequiredState req_res_dustcart dustcart_res_capacity::used)
 (RequiredState req_res_elevator elevator_res_capacity::used)

 # NOT CONCURRENT
 (Constraint During(req_c6b,req_res_elevator))
 (Constraint During(req_c6,req_res_elevator))
 (Constraint During(req_c7,req_res_elevator))
 (Constraint During(req_c9,req_res_elevator))
 (Constraint During(req_c10,req_res_elevator))
 (Constraint During(req_c11,req_res_elevator))
 (Constraint During(req_a1b,req_res_elevator))
 (Constraint During(req6,req_res_elevator))
 (Constraint During(req7,req_res_elevator))
 (Constraint During(req9,req_res_elevator))
 (Constraint During(req10,req_res_elevator))
 (Constraint During(req11,req_res_elevator))
 (Constraint During(req13,req_res_elevator))

 (Constraint During(req_c6b,req_res_dustcart))
 (Constraint During(req_c6,req_res_dustcart))
 (Constraint During(req_c7,req_res_dustcart))
 (Constraint During(req_c9,req_res_dustcart))
 (Constraint During(req_c10,req_res_dustcart))
 (Constraint During(req_c11,req_res_dustcart))
 (Constraint During(req_a1b,req_res_dustcart))
 (Constraint During(req6,req_res_dustcart))
 (Constraint During(req7,req_res_dustcart))
 (Constraint During(req9,req_res_dustcart))
 (Constraint During(req10,req_res_dustcart))
 (Constraint During(req11,req_res_dustcart))
 (Constraint During(req13,req_res_dustcart))

 (Constraint During(req_c6b,req_res_coro))
 (Constraint During(req_c6,req_res_coro))
 (Constraint During(req_c7,req_res_coro))
 (Constraint During(req_c9,req_res_coro))
 (Constraint During(req_c10,req_res_coro))
 (Constraint During(req_c11,req_res_coro))
 (Constraint During(req_a1b,req_res_coro))
 (Constraint During(req6,req_res_coro))
 (Constraint During(req7,req_res_coro))
 (Constraint During(req9,req_res_coro))
 (Constraint During(req10,req_res_coro))
 (Constraint During(req11,req_res_coro))
 (Constraint During(req13,req_res_coro))

 (Constraint During(req0a,req_res_dustcart))
 (Constraint During(req0,req_res_dustcart))
 (Constraint During(req3,req_res_dustcart))
 (Constraint During(req5,req_res_dustcart))
 (Constraint During(req_b1,req_res_dustcart))
 (Constraint During(req_b2,req_res_dustcart))
 (Constraint During(req_b3,req_res_dustcart))

 (Constraint During(req0a,req_res_elevator))
 (Constraint During(req0,req_res_elevator))
 (Constraint During(req3,req_res_elevator))
 (Constraint During(req5,req_res_elevator))
 (Constraint During(req_b1,req_res_elevator))
 (Constraint During(req_b2,req_res_elevator))
 (Constraint During(req_b3,req_res_elevator))

 (Constraint During(req0a,req_res_coro))
 (Constraint During(req0,req_res_coro))
 (Constraint During(req3,req_res_coro))
 (Constraint During(req5,req_res_coro))
 (Constraint During(req_b1,req_res_coro))
 (Constraint During(req_b2,req_res_coro))
 (Constraint During(req_b3,req_res_coro))

 (Constraint During(req_c6a,req_res_coro))
 (Constraint During(req_c11a,req_res_coro))
 (Constraint During(req_c12,req_res_coro))
 (Constraint During(req_a1,req_res_coro))
 (Constraint During(req6a,req_res_coro))
 (Constraint During(req11a,req_res_coro))
 (Constraint During(req12,req_res_coro))
 (Constraint During(req,req_res_coro))
 (Constraint During(req1,req_res_coro))
 (Constraint During(req2,req_res_coro))
 (Constraint During(req4,req_res_coro))
 (Constraint During(req_b0,req_res_coro))

 (Constraint During(req_c6a,req_res_elevator))
 (Constraint During(req_c11a,req_res_elevator))
 (Constraint During(req_c12,req_res_elevator))
 (Constraint During(req_a1,req_res_elevator))
 (Constraint During(req6a,req_res_elevator))
 (Constraint During(req11a,req_res_elevator))
 (Constraint During(req12,req_res_elevator))
 (Constraint During(req,req_res_elevator))
 (Constraint During(req1,req_res_elevator))
 (Constraint During(req2,req_res_elevator))
 (Constraint During(req4,req_res_elevator))
 (Constraint During(req_b0,req_res_elevator))

 (Constraint During(req_c6a,req_res_dustcart))
 (Constraint During(req_c11a,req_res_dustcart))
 (Constraint During(req_c12,req_res_dustcart))
 (Constraint During(req_a1,req_res_dustcart))
 (Constraint During(req6a,req_res_dustcart))
 (Constraint During(req11a,req_res_dustcart))
 (Constraint During(req12,req_res_dustcart))
 (Constraint During(req,req_res_dustcart))
 (Constraint During(req1,req_res_dustcart))
 (Constraint During(req2,req_res_dustcart))
 (Constraint During(req4,req_res_dustcart))
 (Constraint During(req_b0,req_res_dustcart))

)

# Service: shopping delivery
(SimpleOperator
 (Head inference.goal.type::DELIVERY_SERVICE)
 
 (RequiredState req_a0 in.goal.type::DELIVERY_SERVICE)

 (RequiredState req_a0a dustcart::moveto(corner1.pos.gps))
 (RequiredState req_a1 dustcart::moveto(shop.pos.gps))
# (RequiredState req_a1a ws::notification_shop(HI_Shop))
 (RequiredState req_a2 dustcart::startgui(TAKEGOODS))
 (RequiredState req_a2a dustcart::moveto(corner2.pos.gps))
 (RequiredState req_a3 dustcart::moveto(home.pos.gps))

 # EVERYTHING BELOW IS THE GOODS-EXCHANGE, CORO--DUSTCART.
 (RequiredState req coro::moveto(meetingpoint))
 (RequiredState req0 dustcart::dock(DOCK))
 (RequiredState req1 coro::miradock(DUSTCART))
 (RequiredState req2 coro::roller(LOAD))
 (RequiredState req3 dustcart::bin(UNLOAD))
 (RequiredState req4 coro::miradock(UNDOCK))
 (RequiredState req5 dustcart::dock(UNDOCK))
 
 # EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req6b elevator::door(CLOSE))
 (RequiredState req6 elevator::moveto(FLOOR0))
 (RequiredState req7 elevator::door(OPEN))
 (RequiredState req6a coro::moveto(elevator_floor0))
 (RequiredState req9 elevator::door(CLOSE))
 (RequiredState req10 elevator::moveto(FLOOR1))
 (RequiredState req11 elevator::door(OPEN))
 (RequiredState req11a coro::loadmap(FLOOR1))
 (RequiredState req12 coro::moveto(outside_elevator_floor1))

 # Below this is - coro going to the Apartment
 (RequiredState req_b0 coro::moveto(door1))
 (RequiredState req_b1 elevator::door(CLOSE))

 # Below this concerns doro and web server for interaction with user
# (RequiredState req_c0 ws::interaction_doro(SHOP_ARRIVED))

 (Constraint Duration[3000,INF](req_a0a))
 (Constraint Duration[3000,INF](req_a1))
# (Constraint Duration[3000,INF](req_a1a))
 (Constraint Duration[3000,INF](req_a2))
 (Constraint Duration[3000,INF](req_a2a))
 (Constraint Duration[3000,INF](req_a3))

 (Constraint Duration[3000,INF](req))
 (Constraint Duration[3000,INF](req0))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))

 (Constraint Duration[3000,INF](req6b))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req6a))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req11a))
 (Constraint Duration[3000,INF](req12))

 (Constraint Duration[3000,INF](req_b0))
 (Constraint Duration[3000,INF](req_b1))

# (Constraint Duration[3000,INF](req_c0))

 (Constraint Starts[10,10](Head,req_a0))

 (Constraint Meets(req_a0a,req_a1))
# (Constraint Meets(req_a1,req_a1a))
# (Constraint Meets(req_a1a,req_a2))
 (Constraint Meets(req_a1,req_a2))
# (Constraint Meets(req_a2,req_a3))
 (Constraint Meets(req_a2,req_a2a))
 (Constraint Meets(req_a2a,req_a3))

 (Constraint Before(req_a2,req))
 (Constraint Before(req_a3,req0))

 (Constraint Before(req,req0))
 (Constraint Meets(req0,req1))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req2,req4))
 (Constraint Before(req3,req5))
 (Constraint Before(req4,req5))

 (Constraint Before(req5,req6b))

 (Constraint Meets(req6b,req6))
 (Constraint Meets(req6,req7))
 (Constraint Before(req7,req6a))
 (Constraint Meets(req6a,req9))
# (Constraint Before(req7,req9))
 (Constraint Meets(req9,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req9,req11a))
 (Constraint Before(req11a,req12))
 (Constraint Before(req11,req12))
 
 (Constraint Meets(req12,req_b1))
 (Constraint Meets(req12,req_b0))

# (Constraint Meets(req_b0,req_c0))

 (RequiredState req_res_coro coro_res_capacity::used)
 (RequiredState req_res_dustcart dustcart_res_capacity::used)
 (RequiredState req_res_elevator elevator_res_capacity::used)

 # NOT CONCURRENT
 (Constraint During(req6b,req_res_elevator))
 (Constraint During(req6,req_res_elevator))
 (Constraint During(req7,req_res_elevator))
 (Constraint During(req9,req_res_elevator))
 (Constraint During(req10,req_res_elevator))
 (Constraint During(req11,req_res_elevator))
 (Constraint During(req_b1,req_res_elevator))

 (Constraint During(req6b,req_res_coro))
 (Constraint During(req6,req_res_coro))
 (Constraint During(req7,req_res_coro))
 (Constraint During(req9,req_res_coro))
 (Constraint During(req10,req_res_coro))
 (Constraint During(req11,req_res_coro))
 (Constraint During(req_b1,req_res_coro))

 (Constraint During(req6b,req_res_dustcart))
 (Constraint During(req6,req_res_dustcart))
 (Constraint During(req7,req_res_dustcart))
 (Constraint During(req9,req_res_dustcart))
 (Constraint During(req10,req_res_dustcart))
 (Constraint During(req11,req_res_dustcart))
 (Constraint During(req_b1,req_res_dustcart))


 (Constraint During(req_a0a,req_res_dustcart))
 (Constraint During(req_a1,req_res_dustcart))
 (Constraint During(req_a2,req_res_dustcart))
 (Constraint During(req_a2a,req_res_dustcart))
 (Constraint During(req_a3,req_res_dustcart))
 (Constraint During(req0,req_res_dustcart))
 (Constraint During(req3,req_res_dustcart))
 (Constraint During(req5,req_res_dustcart))

 (Constraint During(req_a0a,req_res_coro))
 (Constraint During(req_a1,req_res_coro))
 (Constraint During(req_a2,req_res_coro))
 (Constraint During(req_a2a,req_res_coro))
 (Constraint During(req_a3,req_res_coro))
 (Constraint During(req0,req_res_coro))
 (Constraint During(req3,req_res_coro))
 (Constraint During(req5,req_res_coro))

 (Constraint During(req_a0a,req_res_elevator))
 (Constraint During(req_a1,req_res_elevator))
 (Constraint During(req_a2,req_res_elevator))
 (Constraint During(req_a2a,req_res_elevator))
 (Constraint During(req_a3,req_res_elevator))
 (Constraint During(req0,req_res_elevator))
 (Constraint During(req3,req_res_elevator))
 (Constraint During(req5,req_res_elevator))


 (Constraint During(req,req_res_coro))
 (Constraint During(req1,req_res_coro))
 (Constraint During(req2,req_res_coro))
 (Constraint During(req4,req_res_coro))
 (Constraint During(req6a,req_res_coro))
 (Constraint During(req11a,req_res_coro))
 (Constraint During(req12,req_res_coro))
 (Constraint During(req_b0,req_res_coro))

 (Constraint During(req,req_res_elevator))
 (Constraint During(req1,req_res_elevator))
 (Constraint During(req2,req_res_elevator))
 (Constraint During(req4,req_res_elevator))
 (Constraint During(req6a,req_res_elevator))
 (Constraint During(req11a,req_res_elevator))
 (Constraint During(req12,req_res_elevator))
 (Constraint During(req_b0,req_res_elevator))

 (Constraint During(req,req_res_dustcart))
 (Constraint During(req1,req_res_dustcart))
 (Constraint During(req2,req_res_dustcart))
 (Constraint During(req4,req_res_dustcart))
 (Constraint During(req6a,req_res_dustcart))
 (Constraint During(req11a,req_res_dustcart))
 (Constraint During(req12,req_res_dustcart))
 (Constraint During(req_b0,req_res_dustcart))

)


# (SimpleOperator
#  (Head inference.goal.type::NOOP)
#  (RequiredState req0 in.goal.type::NULL)
#  (RequiredState req_res_doro doro_res_capacity::used)
#  (RequiredState req_res_ws ws_res_capacity::used)
#  (Constraint Duration[3000,INF](Head))
#  (Constraint Starts[10,10](Head,req0))
#  (Constraint During(Head,req_res_doro))
#  (Constraint During(Head,req_res_ws))
# )

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)

 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))

)


# End of service for doro
(SimpleOperator
 (Head inference.goal.type::NO_REQUEST)

 (RequiredState req0 in.goal.type::NO_REQUEST)

# (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(doro_home))

 (RequiredState req_res_doro doro_res_capacity::used)

# (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))

 (Constraint Starts[10,10](Head,req0))

# (Constraint Meets(req1,req2))

# (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))

)


##############################################################

(SimpleOperator

(Head doro_res_capacity::used)

(RequiredResource doro_resource(1))

)

(SimpleOperator

(Head ws_res_capacity::used)

(RequiredResource ws_resource(1))

)

(SimpleOperator

(Head elevator_res_capacity::used)

(RequiredResource elevator_resource(1))

)

(SimpleOperator

(Head coro_res_capacity::used)

(RequiredResource coro_resource(1))

)

(SimpleOperator

(Head dustcart_res_capacity::used)

(RequiredResource dustcart_resource(1))

)
