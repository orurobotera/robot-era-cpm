##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain ShoppingDelivery)





########################
# sensor tuples on CAM #
########################

# The type of goal. Eg: DELIVERY_SERVICE
(Sensor in.goal.type)
(Sensor in.goal.args)

# Position of the user
(Sensor user1.pos.geo)

# Locations used by Dustcart.
(Sensor home.pos.gps)
(Sensor shop.pos.gps)

##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)



#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator dustcart)
(Actuator coro)
(Actuator elevator)
(Actuator ws)


######################
# Planning operators #
######################

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)

(SimpleOperator
 (Head inference.goal.type::DELIVERY_SERVICE)
 
 (RequiredState req_a0 in.goal.type::DELIVERY_SERVICE)
 (RequiredState req_a1 dustcart::moveto(shop.pos.gps))
 (RequiredState req_a1a ws::notification_shop(HI_Shop))
 (RequiredState req_a2 dustcart::startgui(TAKEGOODS))
 (RequiredState req_a3 dustcart::moveto(home.pos.gps))
 
 # EVERYTHING BELOW IS THE GOODS-EXCHANGE, CORO--DUSTCART.
 (RequiredState req coro::moveto(meetingpoint))
 (RequiredState req0 dustcart::dock(DOCK))
 (RequiredState req1 coro::miradock(DUSTCART))
 (RequiredState req2 coro::roller(LOAD))
 (RequiredState req3 dustcart::bin(UNLOAD))
 (RequiredState req4 coro::miradock(UNDOCK))
 (RequiredState req5 dustcart::dock(UNDOCK))
 
 # EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req6 elevator::moveto(FLOOR0))
 (RequiredState req7 elevator::door(OPEN))
 (RequiredState req6a coro::moveto(elevator_floor0))
 (RequiredState req9 elevator::door(CLOSE))
 (RequiredState req10 elevator::moveto(FLOOR1))
 (RequiredState req11 elevator::door(OPEN))
 (RequiredState req11a coro::moveto(outside_elevator_floor1))
 (RequiredState req12 coro::loadmap(FLOOR1))
 
 # Below this is - coro going to the Apartment
 (RequiredState req_b0 coro::moveto(door1))
 (RequiredState req_b1 elevator::door(CLOSE))

 # Below this concerns doro and web server for interaction with user
 (RequiredState req_c0 ws::interaction_doro(SHOP_ARRIVED))
 
 (Constraint Duration[300,INF](Head))

 (Constraint Duration[3000,INF](req_a1))
 (Constraint Duration[3000,INF](req_a1a))
 (Constraint Duration[3000,INF](req_a2))
 (Constraint Duration[3000,INF](req_a3)) 
 (Constraint Duration[3000,INF](req))
 (Constraint Duration[3000,INF](req0))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req6a))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req11a))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req_b0))
 (Constraint Duration[3000,INF](req_b1))
 (Constraint Duration[3000,INF](req_c0))
 
 (Constraint Starts[10,10](Head,req_a0))
 (Constraint Meets(req_a1,req_a1a))
 (Constraint Meets(req_a1a,req_a2))
 (Constraint Meets(req_a2,req_a3))
 (Constraint Meets(req_a3,req0))
 (Constraint Before(req,req0))
 (Constraint Meets(req0,req1))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req2,req4))
 (Constraint Before(req4,req5))

 (Constraint Before(req5,req6a))

 (Constraint Before(req6,req6a))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req6a,req9))
 (Constraint Before(req7,req9))
 (Constraint Meets(req9,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req11,req11a))
 (Constraint Meets(req11a,req12))
 
 (Constraint Meets(req11a,req_b1))
 (Constraint Meets(req12,req_b0))

 (Constraint Meets(req_b0,req_c0))
)

)
