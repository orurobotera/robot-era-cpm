##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain Communication)



# sensor tuples on CAM
(Sensor user1.pos.geo)
(Sensor in.goal.type)
(Sensor in.goal.args)

# desired states (goals) should be inferred for doro
(ContextVariable inference.doro.goal.type)

#############
# Actuators #
#############

# doro's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)


######################
# Planning operators #
######################

# User calls doro
(SimpleOperator
 (Head inference.doro.goal.type::VIDEOCALL_REQUEST)
 (RequiredState req0 in.goal.type::VIDEOCALL_REQUEST)

 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(INCOMING_CALL))

# NEW SERVICE REQUESTED?
 (RequiredState req_e ws::interaction_doro(USER_SERVICE_REQUEST))

 (Constraint Duration[300,INF](Head))

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Duration[3000,INF](req_e))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint Meets(req4,req_e))

)

# End of service for doro
(SimpleOperator
 (Head inference.doro.goal.type::NO_REQUEST)
 (RequiredState req0 in.goal.type::NO_REQUEST)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(ROBOT_HOME))

 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))

)
