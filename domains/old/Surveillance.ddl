##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain Surveillance)


# sensor tuples on CAM
(Sensor user1.pos.geo)
(Sensor in.goal.type)
# arguments may be either GAS or DOOR
(Sensor in.goal.args)

# desired states (goals) should be inferred for doro
(ContextVariable inference.doro.goal.type)

#############
# Actuators #
#############

# doro's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)


#####################
# Planning operator #
#####################

# Doro warns the user
(SimpleOperator
 (Head inference.doro.goal.type::EMERGENCY_SERVICE)
 (RequiredState req0 in.goal.type::EMERGENCY_SERVICE)

 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::notification_doro(in.goal.args))

 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)
