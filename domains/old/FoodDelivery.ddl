##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain FoodDelivery)


# sensor tuples on CAM
(Sensor in.goal.type)
(Sensor in.goal.args)

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)

# Robots and web server
(Actuator doro)
(Actuator coro)
(Actuator ws)


# PLANNING OPERATORS used for inferring desired goals and planning

# doro goes to the user and interacts with them
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1))
 (RequiredState req2 doro::finduser(USER1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3,req4))
)

# food delivery with the help of doro and coro
(SimpleOperator
 (Head inference.goal.type::BRING_FOOD)
 (RequiredState req0 in.goal.type::BRING_FOOD)
 (RequiredState req1 ws::interaction_doro(USER_CHOOSE_MENU))
 (RequiredState req2 doro::dock(UNDOCK))
 (RequiredState req3 coro::moveto(box))
 (RequiredState req4 coro::pickup(BOX1))
 (RequiredState req5 coro::moveto(TABLE1))
 (RequiredState req6 coro::putdown(BOX1))
 (RequiredState req7 doro::moveto(TABLE1))
 (RequiredState req8 doro::look(BOX1))
 (RequiredState req9 doro::acquire(BOX1))
 (RequiredState req10 doro::dock(BOX1))
 (RequiredState req11 doro::pickup(BOX1))
 (RequiredState req12 doro::dock(UNDOCK))
 (RequiredState req13 doro::moveto(user1))
 (RequiredState req14 doro::putdown(BOX1))
 (RequiredState req15 ws::notification_doro(FOOD_DELIVERED))
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req13))
 (Constraint Duration[3000,INF](req14))
 (Constraint Duration[3000,INF](req15))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
 (Constraint Meets(req4,req5))
 (Constraint Meets(req5,req6))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req7,req8))
 (Constraint Meets(req8,req9))
 (Constraint Meets(req9,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req11,req12))
 (Constraint Meets(req12,req13))
 (Constraint Meets(req13,req14))
 (Constraint Meets(req14,req15))
)
