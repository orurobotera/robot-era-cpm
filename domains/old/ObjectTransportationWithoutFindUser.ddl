##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain ObjectTransportationWithoutFindUser)



# sensor tuples on CAM
(Sensor user1.pos.geo)
(Sensor in.goal.type)
(Sensor in.goal.args)

# desired states (goals) should be inferred for doro
(ContextVariable inference.doro.goal.type)

#############
# Actuators #
#############

# doro's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)


######################
# Planning operators #
######################

# User calls doro
(SimpleOperator
 (Head inference.doro.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1.pos.geo))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)

# operators used for inferring desired goals and planning
(SimpleOperator
 (Head inference.doro.goal.type::BRING)
 (RequiredState req0 in.goal.type::BRING)
 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(in.goal.args))
 (RequiredState req3 doro::look(in.goal.args))
 (RequiredState req4 doro::acquire(in.goal.args))
 (RequiredState req5 doro::dock(in.goal.args))
 (RequiredState req6 doro::pickup(in.goal.args))
 (RequiredState req7 doro::dock(UNDOCK))
 (RequiredState req8 doro::moveto(user1.pos.geo))
 (RequiredState req10 doro::handover(EXTEND))
 (RequiredState req11 ws::notification_doro(TAKE_OBJECT))
 (RequiredState req12 doro::handover(WAIT))
 (RequiredState req13 doro::handover(RETRACT))

# NEW SERVICE REQUESTED?
 (RequiredState req_e ws::interaction_doro(USER_SERVICE_REQUEST))

 (Constraint Duration[300,INF](Head))

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req12))
 (Constraint Duration[3000,INF](req13))

 (Constraint Duration[3000,INF](req_e))

 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3,req4))
 (Constraint Meets(req4,req5))
 (Constraint Meets(req5,req6))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req7,req8))
 (Constraint Meets(req8,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req10,req12))
 (Constraint Meets(req12,req13))

 (Constraint Meets(req13,req_e))

)

# End of service for doro
(SimpleOperator
 (Head inference.goal.type::NO_REQUEST)
 (RequiredState req0 in.goal.type::NO_REQUEST)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(ROBOT_HOME))

 (Constraint Duration[300,INF](Head))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))

)
