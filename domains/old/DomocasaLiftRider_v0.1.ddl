##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain DomocasaLiftRider)

# sensor tuples on CAM
(Sensor elevator.pos.geo.floor0)
(Sensor elevator.pos.geo.floor1)
(Sensor out.goal.type)

# desired states (goals) should be inferred for doro
(ContextVariable inference.elevator.goal.type)

# doro's states cannot occur in the past (they are actions to be executed)
(Actuator coro)
(Actuator elevator)

# operators used for inferring desired goals and planning
(SimpleOperator
 (Head inference.elevator.goal.type::RIDE_THE_ELEVATOR)
 (RequiredState req0 out.goal.type::RIDE_THE_ELEVATOR)
 (RequiredState req1 elevator::moveto(FLOOR1))
 (RequiredState req2 elevator::door(OPEN))
 (RequiredState req3 coro::miradock(ELEVATOR))
 (RequiredState req4 elevator::door(CLOSE))
 (RequiredState req5 elevator::moveto(FLOOR0))
 (RequiredState req6 coro::loadmap(FLOOR0))
 (RequiredState req7 elevator::door(OPEN))
 (RequiredState req8 coro::moveto(0.47, 2.32, 0, 0.2, 0.2, -1))
 (RequiredState req9 elevator::door(CLOSE))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Duration[3000,INF](Head))
 (Constraint Duration[3000,INF](req0))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))
 (Constraint Duration[3000,INF](req9))
 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3,req4))
 (Constraint Meets(req4,req5))
 (Constraint Meets(req5,req6))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req7,req8))
 (Constraint Meets(req8,req9))
)