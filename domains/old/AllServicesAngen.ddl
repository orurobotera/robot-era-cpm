##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain AllServicesAngen)


# (TimelinesToShow Time doro coro inference.goal.type in.goal.type in.goal.args home.pos.gps shop.pos.gps)
# (TimelinesToShow Time doro ws inference.goal.type in.goal.type in.goal.args home.pos.gps shop.pos.gps)
# (TimelinesToShow Time coro doro elevator ws home.pos.gps in.goal.type in.goal.args inference.goal.type)

#(UrgentRequests EMERGENCY_SERVICE VIDEOCALL_REQUEST REMIND_EVENT)



########################
# sensor tuples on CAM #
########################

# The type of goal.
# So far: REACH_USER_REQUEST, BRING, EMERGENCY_SERVICE, VIDEOCALL_REQUEST, NO_REQUEST, ESCORT_USER, REMIND_EVENT, LAUNDRY_LOAD
(Sensor in.goal.type)
(Sensor in.goal.args)



##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)


#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator coro)
(Actuator elevator)
(Actuator ws)


###################
# Unary resources #
###################

(Resource doro_resource 1)
(Resource coro_resource 1)
(Resource elevator_resource 1)
(Resource ws_resource 1)


######################
# Planning operators #
######################


# (SimpleOperator
#  (Head inference.goal.type::NOOP)
#  (RequiredState req0 in.goal.type::null)

#  (RequiredState req_res_doro doro_res_capacity::used)
#  (RequiredState req_res_ws ws_res_capacity::used)
#  (RequiredState req_res_coro coro_res_capacity::used)
#  (RequiredState req_res_elevator elevator_res_capacity::used)

#  (Constraint Duration[3000,INF](Head))

#  # (Constraint Starts[10,10](Head,req0))

#  (Constraint During(Head,req_res_doro))
#  (Constraint During(Head,req_res_ws))
#  (Constraint During(Head,req_res_coro))
#  (Constraint During(Head,req_res_elevator))

# )

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)

 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req4,req_res_doro))

)

# Service: laundry transport
# Transport laundry, wash it, and go back to user with clean clothes
(SimpleOperator
 (Head inference.goal.type::LAUNDRY_LOAD)

 (RequiredState req0 in.goal.type::LAUNDRY_LOAD)

# TRAVEL TO USER, LOAD LAUNDRY
 (RequiredState req_a1 coro::moveto(door1))
 (RequiredState req_a2 ws::notification_doro(CORO_AT_ENTRANCE))
# action ws::interation_coro(HI_Condominium) is not implemented
# instead of it, use of timer (e.g. 3min) to be replaced later
# by an interaction between doro and the user
# (RequiredState req_a3 ws::interaction_coro(HI_Condominium))

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req6 elevator::moveto(FLOOR2))
# (RequiredState req7 elevator::door(OPEN))
# no need for elevator::door(OPEN), since elevator::moveto(XXX)
# ends immediately; a coro behavior is to be used instead 
# for detecting that door is open.
 (RequiredState req7 coro::moveto(outside_elevator_floor2))
 (RequiredState req8 coro::lift(ENTER floor2))

# unnecessary, since the door closes automatically after a time delay
# (RequiredState req9 elevator::door(CLOSE))

 (RequiredState req10 elevator::moveto(FLOOR6))
 (RequiredState req11 coro::loadmap(FLOOR6))
 (RequiredState req12 coro::lift(EXIT floor6))

# Below this is - coro going to the laundry facility
 (RequiredState req_b0 coro::moveto(launderette))

 (RequiredState req_b1 coro::wait(LAUNDRY))

# EVERYTHING BELOW IS THE ENTER EXIT ELEVATOR, CORO.
 (RequiredState req_c6 elevator::moveto(FLOOR6))
 (RequiredState req_c7 coro::moveto(outside_elevator_floor6))
 (RequiredState req_c8 coro::lift(ENTER floor6))
 (RequiredState req_c10 elevator::moveto(FLOOR2))
 (RequiredState req_c11 coro::loadmap(FLOOR2))
 (RequiredState req_c12 coro::lift(EXIT floor2))

# TRAVEL BACK TO USER, UNLOAD LAUNDRY
 (RequiredState req_d1 coro::moveto(door1))
 (RequiredState req_d2 ws::notification_doro(CORO_AT_ENTRANCE))
# (RequiredState req_d3 ws::interaction_coro(HI_Condominium))

 (RequiredState req_res_coro coro_res_capacity::used)
 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)
 (RequiredState req_res_elevator elevator_res_capacity::used)
 
 (Constraint Duration[3000,INF](req_a1))
 (Constraint Duration[3000,INF](req_a2))
# (Constraint Duration[3000,INF](req_a3))

 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))
# (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req12))

 (Constraint Duration[3000,INF](req_b0))
 (Constraint Duration[3000,INF](req_b1))

 (Constraint Duration[3000,INF](req_c6))
 (Constraint Duration[3000,INF](req_c7))
 (Constraint Duration[3000,INF](req_c8))
 (Constraint Duration[3000,INF](req_c10))
 (Constraint Duration[3000,INF](req_c11))
 (Constraint Duration[3000,INF](req_c12))

 (Constraint Duration[3000,INF](req_d1))
 (Constraint Duration[3000,INF](req_d2))
# (Constraint Duration[3000,INF](req_d3))


 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req_a1,req_a2))
# (Constraint Meets(req_a2,req_a3))

# (Constraint Meets(req_a3,req6a))
 (Constraint Before(req_a2,req7))

 (Constraint Before(req6,req8))
 (Constraint Before(req6,req10))
 (Constraint Meets(req7,req8))
 (Constraint Before(req8,req10))
 (Constraint Meets(req8,req11))
 (Constraint Before(req10,req12))
 (Constraint Before(req11,req12))

 (Constraint Before(req10,req_c6))

 (Constraint Meets(req12,req_b0))

 (Constraint Meets(req_b0,req_b1))

 (Constraint Meets(req_b1,req_c7))

 (Constraint Before(req_c6,req_c8))
 (Constraint Before(req_c6,req_c10))
 (Constraint Meets(req_c7,req_c8))
 (Constraint Before(req_c8,req_c10))
 (Constraint Meets(req_c8,req_c11))
 (Constraint Before(req_c10,req_c12))
 (Constraint Before(req_c11,req_c12))

 (Constraint Meets(req_c12,req_d1))

 (Constraint Meets(req_d1,req_d2))
# (Constraint Meets(req_d2,req_d3))

 (Constraint During(req_a1,req_res_coro))
 (Constraint During(req_a2,req_res_ws))
 (Constraint During(req_a2,req_res_doro))
# (Constraint During(req_a3,req_res_ws))

 (Constraint During(req6,req_res_elevator))
 (Constraint During(req7,req_res_coro))
 (Constraint During(req8,req_res_coro))
 (Constraint During(req10,req_res_elevator))
 (Constraint During(req11,req_res_coro))
 (Constraint During(req12,req_res_coro))

 (Constraint During(req_b0,req_res_coro))

 (Constraint During(req_b1,req_res_coro))

 (Constraint During(req_c6,req_res_elevator))
 (Constraint During(req_c7,req_res_coro))
 (Constraint During(req_c8,req_res_coro))
 (Constraint During(req_c10,req_res_elevator))
 (Constraint During(req_c11,req_res_coro))
 (Constraint During(req_c12,req_res_coro))

 (Constraint During(req_d1,req_res_coro))
 (Constraint During(req_d2,req_res_ws))
 (Constraint During(req_d2,req_res_doro))
# (Constraint During(req_d3,req_res_ws))

)



# doro brings an object to the user
(SimpleOperator
 (Head inference.goal.type::BRING)

 (RequiredState req0 in.goal.type::BRING)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(table1))
 (RequiredState req3 doro::look(in.goal.args))
 (RequiredState req4 doro::acquire(in.goal.args))
 (RequiredState req5 doro::dock(in.goal.args))
 (RequiredState req6 doro::pickup(in.goal.args))
 (RequiredState req7 doro::dock(UNDOCK))
 (RequiredState req8 doro::moveto(user1))
 (RequiredState req10 doro::handover(EXTEND))
 (RequiredState req11 ws::notification_doro(TAKE_OBJECT))
 (RequiredState req12 doro::handover(WAIT))
# (RequiredState req13 doro::handover(RETRACT))

# NEW SERVICE REQUESTED?
 (RequiredState req_e ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req12))
# (Constraint Duration[3000,INF](req13))

 (Constraint Duration[3000,INF](req_e))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3,req4))
 (Constraint Meets(req4,req5))
 (Constraint Meets(req5,req6))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req7,req8))
 (Constraint Meets(req8,req10))
 (Constraint Meets(req10,req11))
 (Constraint Meets(req10,req12))
# (Constraint Meets(req12,req13))

# (Constraint Meets(req13,req_e))
 (Constraint Meets(req12,req_e))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req5,req_res_doro))
 (Constraint During(req6,req_res_doro))
 (Constraint During(req7,req_res_doro))
 (Constraint During(req8,req_res_doro))
 (Constraint During(req10,req_res_doro))
 (Constraint During(req11,req_res_ws))
 (Constraint During(req12,req_res_doro))
 # (Constraint During(req13,req_res_doro))
 (Constraint During(req_e,req_res_ws))

)

# doro warns the user
(SimpleOperator
 (Head inference.goal.type::EMERGENCY_SERVICE)

 (RequiredState req0 in.goal.type::EMERGENCY_SERVICE)

 (RequiredState req1 doro::moveto(user1))
# (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::notification_doro(in.goal.args))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 # (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 # (Constraint Meets(req1,req3))
 # (Constraint Meets(req3,req4))
 (Constraint Meets(req1,req4))

 (Constraint During(req1,req_res_doro))
 # (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_ws))

)

# Incoming video call for the user
(SimpleOperator
 (Head inference.goal.type::VIDEOCALL_REQUEST)

 (RequiredState req0 in.goal.type::VIDEOCALL_REQUEST)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(INCOMING_CALL))

# NEW SERVICE REQUESTED?
 (RequiredState req_e ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Duration[3000,INF](req_e))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint Meets(req4,req_e))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_ws))

 (Constraint During(req_e,req_res_ws))

)

# End of service for doro
(SimpleOperator
 (Head inference.goal.type::NO_REQUEST)

 (RequiredState req0 in.goal.type::NO_REQUEST)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(doro_home))

 (RequiredState req_res_doro doro_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))

)

# doro escorts the user who holds it
(SimpleOperator
 (Head inference.goal.type::ESCORT_USER)
 (RequiredState req0 in.goal.type::ESCORT_USER)
 (RequiredState req1 doro::handle(ENABLE))
 (RequiredState req_res_doro doro_res_capacity::used)
 (Constraint Duration[3000,INF](req1))
 (Constraint Starts[10,10](Head,req0))
 (Constraint During(req1,req_res_doro))
)

# doro goes to the user and reminds them to take some drugs
(SimpleOperator
 (Head inference.goal.type::REMIND_EVENT)

 (RequiredState req0 in.goal.type::REMIND_EVENT)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::notification_doro(in.goal.args))

# NEW SERVICE REQUESTED?
 (RequiredState req_e ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Duration[3000,INF](req_e))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint Meets(req4,req_e))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req_e,req_res_ws))

)




##############################################################

(SimpleOperator

(Head doro_res_capacity::used)

(RequiredResource doro_resource(1))

)

(SimpleOperator

(Head coro_res_capacity::used)

(RequiredResource coro_resource(1))

)

(SimpleOperator

(Head elevator_res_capacity::used)

(RequiredResource elevator_resource(1))

)

(SimpleOperator

(Head ws_res_capacity::used)

(RequiredResource ws_resource(1))

)
