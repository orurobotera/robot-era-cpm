##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   SimpleDomain                                                #
#   Constraint                                                  #
#   RequiredState					                        	#
#   AchievedState			                        			#
#   RequiredResource	                    					#
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain TestPlanningPEIS)

# symbolic <-> metric locations
# Note: this has to be removed, it should come from the CAM
(Location LR_C1 0.13 0.71 -0.52)
(Location LR_C2 3.93 0.62 1.1)
(Location LR_C2 5.37 1.62 0.5)

# PEIS tuples for CAM
(Sensor context.user.location.room)
(Sensor doro1.location.room)
(Sensor context.user.state)

# desired states (goals) should be inferred for doro1
(ContextVariable doro1)

# doro1's states cannot occur in the past (they are actions to be executed)
(Actuator doro1)

# operators used for inferring desired goals and planning
(SimpleOperator
 (Head doro1::moveto_LR_C1)
 (RequiredState req1 context.user.state::needs_help)
 (Constraint Duration[3000,INF](Head))
)
