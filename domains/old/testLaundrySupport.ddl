##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   SimpleDomain                                                #
#   Constraint                                                  #
#   RequiredState						                        #
#   AchievedState						                        #
#   RequiredResource                    						#
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain TestLaundrySupport)

# No sensor
# No context recognition

(Actuator Robot)

(SimpleOperator
 (Head Human::Awake())
 (RequiredState req1 Bed::Off())
 (RequiredState req2 Bed::On())
 (RequiredState req3 TimeOfDay::Night())
 (RequiredState req4 Robot::AtBedside())
 (Constraint MetBy(req1,req2))
 (Constraint Finishes(Head,req3))
 (Constraint Finishes(Head,req1))
)

