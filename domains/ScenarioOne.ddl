##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain ScenarioOne)


#(TimelinesToShow Time doro ws in.goal.type inference.goal.type)

#(UrgentRequests EMERGENCY_SERVICE)



########################
# sensor tuples on CAM #
########################

# The type of goal.
# REACH_USER_REQUEST, BRING, EMERGENCY_SERVICE, NO_REQUEST, ESCORT_USER, REMIND_EVENT
(Sensor in.goal.type)


##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)


#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)


###################
# Unary resources #
###################

(Resource doro_resource 1)
(Resource ws_resource 1)


######################
# Planning operators #
######################


# (SimpleOperator

#  (Head inference.goal.type::NOOP)

#  (RequiredState req0 in.goal.type::NULL)

#  (RequiredState req_res_doro doro_res_capacity::used)
#  (RequiredState req_res_ws ws_res_capacity::used)

#  (Constraint Duration[3000,INF](Head))

#  (Constraint Starts[10,10](Head,req0))

#  (Constraint During(Head,req_res_doro))
#  (Constraint During(Head,req_res_ws))

# )

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)

 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))

)



# doro brings an object to the user
(SimpleOperator
 (Head inference.goal.type::BRING)

 (RequiredState req0 in.goal.type::BRING)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(table1))
 (RequiredState req3 doro::look(in.goal.args))
 (RequiredState req4 doro::acquire(in.goal.args))
 (RequiredState req5 doro::dock(in.goal.args))
 (RequiredState req6 doro::pickup(in.goal.args))
 (RequiredState req7 doro::dock(UNDOCK))
 (RequiredState req8 doro::moveto(user1))
 (RequiredState req9 doro::dock(handover_pose))
 (RequiredState req10 doro::handover(EXTEND))
 (RequiredState req11 ws::notification_doro(TAKE_SOMETHING))
# TAKE_MEDICINE if in.goal.args equals pills
# TAKE_WATER if in.goal.args equals water
 (RequiredState req11a ws::interaction_doro(SYSTEM_SERVICE_REQUEST))
 (RequiredState req12 doro::handover(WAIT))


 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))
 (Constraint Duration[3000,INF](req9))
 (Constraint Duration[3000,INF](req10))
 (Constraint Duration[3000,INF](req11))
 (Constraint Duration[3000,INF](req11a))
 (Constraint Duration[3000,INF](req12))


 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))
 (Constraint Meets(req2,req3))
 (Constraint Meets(req3,req4))
 (Constraint Meets(req4,req5))
 (Constraint Meets(req5,req6))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req7,req8))
 (Constraint Meets(req8,req9))
 (Constraint Meets(req8,req10))
 (Constraint Before(req9,req11))
 (Constraint Before(req10,req11))
 (Constraint Before(req9,req12))
 (Constraint Before(req10,req12))
 (Constraint Meets(req12,req11a))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req5,req_res_doro))
 (Constraint During(req6,req_res_doro))
 (Constraint During(req7,req_res_doro))
 (Constraint During(req8,req_res_doro))
 (Constraint During(req9,req_res_doro))
 (Constraint During(req11,req_res_doro))
 (Constraint During(req11,req_res_ws))
 (Constraint During(req11a,req_res_doro))
 (Constraint During(req11a,req_res_ws))
 (Constraint During(req12,req_res_doro))

)

# doro warns the user
(SimpleOperator
 (Head inference.goal.type::EMERGENCY_SERVICE)

 (RequiredState req0 in.goal.type::EMERGENCY_SERVICE)

 (RequiredState req1 doro::moveto(user1))
# (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::warning_doro(in.goal.args))
 (RequiredState req4a ws::interaction_doro(SYSTEM_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 # (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req4a))

 (Constraint Starts[10,10](Head,req0))

 # (Constraint Meets(req1,req3))
 # (Constraint Meets(req3,req4))
 (Constraint Meets(req1,req4))
 (Constraint Meets(req4,req4a))

 (Constraint During(req1,req_res_doro))
 # (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req4a,req_res_doro))
 (Constraint During(req4a,req_res_ws))

)


# End of service for doro
(SimpleOperator
 (Head inference.goal.type::NO_REQUEST)

 (RequiredState req0 in.goal.type::NO_REQUEST)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(doro_home))

 (RequiredState req_res_doro doro_res_capacity::used)

 (Constraint Duration[3000,30000](req1))
 (Constraint Duration[3000,30000](req2))

 (Constraint Starts[10,10](Head,req0))
 (Constraint Before[10,10](Head,req1))
 (Constraint Meets(req1,req2))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))

)

# doro escorts the user who holds it
(SimpleOperator
 (Head inference.goal.type::ESCORT_USER)
 (RequiredState req0 in.goal.type::ESCORT_USER)
 (RequiredState req0a doro::movetosimple(0.0,0.0,3.14))
 (RequiredState req1 doro::handle(ENABLE))
 (RequiredState req2 doro::moveto(doro_home))
 (RequiredState req_res_doro doro_res_capacity::used)
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req0a))
 (Constraint Duration[3000,INF](req2))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req0a,req1))
 (Constraint Meets(req1,req2))
 (Constraint During(req0a,req_res_doro))
 (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))
)

# doro goes to the user and reminds them to take some drugs
# in.goal.parameters contains the relative time in s to start the
# recipe wrt the current time
(SimpleOperator
 (Head inference.goal.type::REMIND_EVENT)

 (RequiredState req0 in.goal.type::REMIND_EVENT)

 (RequiredState req0a doro::wait(in.goal.parameters))
 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::notification_doro(in.goal.args))
 (RequiredState req4a ws::interaction_doro(SYSTEM_SERVICE_REQUEST))

# NEW SERVICE REQUESTED?
# (RequiredState req_e ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

# (Constraint Duration[?remind_time,?remind_time](req0a))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req4a))

# (Constraint Duration[3000,INF](req_e))

 (Constraint Starts[10,10](Head,req0))

# (Constraint Starts[20,20](req0a,req0))
# (Constraint Starts(req0,req0a))

 (Constraint Meets(req0a,req1)) 
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
 (Constraint Meets(req4,req4a))

# (Constraint Meets(req4,req_e))

# (Constraint During(req0a,req_res_doro))
 (Constraint During(req1,req_res_doro))
 (Constraint During(req1,req_res_ws))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req3,req_res_ws))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req4a,req_res_doro))
 (Constraint During(req4a,req_res_ws))
# (Constraint During(req_e,req_res_ws))

)




##############################################################

(SimpleOperator

(Head doro_res_capacity::used)

(RequiredResource doro_resource(1))

)

(SimpleOperator

(Head ws_res_capacity::used)

(RequiredResource ws_resource(1))

)
