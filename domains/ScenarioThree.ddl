##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain ScenarioThree)


#(TimelinesToShow Time coro doro ws in.goal.type inference.goal.type)

#(UrgentRequests EMERGENCY_SERVICE)


########################
# sensor tuples on CAM #
########################

# The type of goal.
# REACH_USER_REQUEST, NO_REQUEST, FOOD_DELIVERY_SERVICE, LAUNDRY_LOAD,
# EMERGENCY_SERVICE
# Missing: CLEANING
(Sensor in.goal.type)

##########################

# desired states (goals) should be inferred for robots
(ContextVariable inference.goal.type)


#############
# Actuators #
#############

# robot's states cannot occur in the past (they are actions to be
# executed)
(Actuator doro)
(Actuator coro)
(Actuator ws)


###################
# Unary resources #
###################

(Resource doro_resource 1)
(Resource coro_resource 1)
(Resource ws_resource 1)


######################
# Planning operators #
######################


# (SimpleOperator
#  (Head inference.goal.type::NOOP)
#  (RequiredState req0 in.goal.type::NULL)
#  (Constraint Starts[10,10](Head,req0))
# )


# doro warns the user
(SimpleOperator
 (Head inference.goal.type::EMERGENCY_SERVICE)

 (RequiredState req0 in.goal.type::EMERGENCY_SERVICE)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req4 ws::warning_doro(in.goal.args))
 (RequiredState req4a ws::interaction_doro(SYSTEM_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req4a))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req4))
 (Constraint Meets(req4,req4a))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req4a,req_res_doro))
 (Constraint During(req4a,req_res_ws))

)


# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)

 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)

 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))

 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req3,req_res_doro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))

)


# Service: laundry transport
# Transport laundry, wash it, and go back to user with clean clothes
(SimpleOperator
 (Head inference.goal.type::LAUNDRY_LOAD)

 (RequiredState req0 in.goal.type::LAUNDRY_LOAD)


# TRAVEL TO USER, LOAD LAUNDRY
 (RequiredState req_a1 coro::moveto(exchange_spot))
 (RequiredState req_a2 ws::notification_doro(CORO_READY_LAUNDRY_LOAD))

 (RequiredState req_b1 coro::moveto(coro_home))

# TRAVEL BACK TO USER, UNLOAD LAUNDRY
 (RequiredState req_c1 coro::moveto(exchange_spot))
 (RequiredState req_c2 ws::notification_doro(CORO_READY_LAUNDRY_UNLOAD))

 (RequiredState req_d1 coro::moveto(coro_home))

# ASK FOR NEW REQUEST
 (RequiredState req_e1 doro::moveto(user1))
 (RequiredState req_e2 doro::dock(user1))
 (RequiredState req_e3 ws::interaction_doro(SYSTEM_SERVICE_REQUEST))


 (RequiredState req_res_coro coro_res_capacity::used)
 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)


 (Constraint Duration[3000,INF](req_a1))
 (Constraint Duration[3000,INF](req_a2))

 (Constraint Duration[3000,INF](req_b1))

 (Constraint Duration[3000,INF](req_c1))
 (Constraint Duration[3000,INF](req_c2))

 (Constraint Duration[3000,INF](req_d1))

 (Constraint Duration[3000,INF](req_e1))
 (Constraint Duration[3000,INF](req_e2))
 (Constraint Duration[3000,INF](req_e3))


 (Constraint Starts[10,10](Head,req0))

 (Constraint Before[60000,INF](Head,req_a1))

 (Constraint Before(req_a1,req_a2))

 (Constraint Before[20000,20000](req_a2,req_b1))

 (Constraint Before[30000,30000](req_b1,req_c1))

 (Constraint Before(req_c1,req_c2))

 (Constraint Before[20000,20000](req_c2,req_d1))
 (Constraint Before[20000,20000](req_c2,req_e1))

# (Constraint Meets(req_d1,req_e1))

 (Constraint Meets(req_e1,req_e2))
 (Constraint Meets(req_e2,req_e3))


 (Constraint During(req_a1,req_res_coro))
 (Constraint During(req_a2,req_res_coro))
 (Constraint During(req_a2,req_res_doro))
 (Constraint During(req_a2,req_res_ws))

 (Constraint During(req_b1,req_res_coro))
 (Constraint During(req_b1,req_res_doro))
 (Constraint During(req_b1,req_res_ws))

 (Constraint During(req_c1,req_res_coro))
 (Constraint During(req_c1,req_res_doro))
 (Constraint During(req_c1,req_res_ws))
 (Constraint During(req_c2,req_res_coro))
 (Constraint During(req_c2,req_res_doro))
 (Constraint During(req_c2,req_res_ws))

 (Constraint During(req_d1,req_res_coro))

 (Constraint During(req_e1,req_res_doro))
 (Constraint During(req_e1,req_res_ws))
 (Constraint During(req_e2,req_res_doro))
 (Constraint During(req_e2,req_res_ws))
 (Constraint During(req_e3,req_res_doro))
 (Constraint During(req_e3,req_res_ws))

)



# food delivery with the help of doro and coro
(SimpleOperator
 (Head inference.goal.type::FOOD_DELIVERY_SERVICE)

 (RequiredState req0 in.goal.type::FOOD_DELIVERY_SERVICE)

 (RequiredState req3 coro::moveto(exchange_spot))
 (RequiredState req4 ws::notification_doro(CORO_READY_FOOD))
 (RequiredState req5 coro::moveto(coro_home))
 (RequiredState req6 doro::moveto(user1))
 (RequiredState req7 doro::dock(user1))
 (RequiredState req8 ws::interaction_doro(SYSTEM_SERVICE_REQUEST))

 (RequiredState req_res_coro coro_res_capacity::used)
 (RequiredState req_res_doro doro_res_capacity::used)
 (RequiredState req_res_ws ws_res_capacity::used)

 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Duration[3000,INF](req5))
 (Constraint Duration[3000,INF](req6))
 (Constraint Duration[3000,INF](req7))
 (Constraint Duration[3000,INF](req8))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Before[60000,INF](Head,req3))

 (Constraint Before(req3,req4))
 (Constraint Before[10000,10000](req4,req5))
 (Constraint Before[10000,10000](req4,req6))
 (Constraint Meets(req6,req7))
 (Constraint Meets(req7,req8))

 (Constraint During(req3,req_res_coro))
 (Constraint During(req4,req_res_coro))
 (Constraint During(req4,req_res_doro))
 (Constraint During(req4,req_res_ws))
 (Constraint During(req5,req_res_coro))
 (Constraint During(req6,req_res_doro))
 (Constraint During(req6,req_res_ws))
 (Constraint During(req7,req_res_doro))
 (Constraint During(req7,req_res_ws))
 (Constraint During(req8,req_res_doro))
 (Constraint During(req8,req_res_ws))
 
)


# End of service for doro
(SimpleOperator
 (Head inference.goal.type::NO_REQUEST)

 (RequiredState req0 in.goal.type::NO_REQUEST)

 (RequiredState req1 doro::dock(UNDOCK))
 (RequiredState req2 doro::moveto(doro_home))

 (RequiredState req_res_doro doro_res_capacity::used)

 (Constraint Duration[3000,30000](req1))
 (Constraint Duration[3000,30000](req2))

 (Constraint Starts[10,10](Head,req0))
 (Constraint Before[10,10](Head,req1))
 (Constraint Meets(req1,req2))

 (Constraint During(req1,req_res_doro))
 (Constraint During(req2,req_res_doro))

)






##############################################################

(SimpleOperator

(Head doro_res_capacity::used)

(RequiredResource doro_resource(1))

)

(SimpleOperator

(Head coro_res_capacity::used)

(RequiredResource coro_resource(1))

)

(SimpleOperator

(Head ws_res_capacity::used)

(RequiredResource ws_resource(1))

)
