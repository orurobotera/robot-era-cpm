package meta.configurationPlanner;

import java.util.logging.Logger;

import org.metacsp.dispatching.DispatchingFunction;
import org.metacsp.framework.Constraint;
import org.metacsp.framework.Variable;
import org.metacsp.multi.activity.ActivityNetworkSolver;
import org.metacsp.multi.activity.SymbolicVariableActivity;
import org.metacsp.multi.allenInterval.AllenIntervalConstraint;
import org.metacsp.utility.logging.MetaCSPLogging;

import core.CallbackObject;
import core.PeisJavaMT;
import core.PeisTuple;
import core.PeisTupleException;

public class RobotEraDispatchingFunction extends DispatchingFunction {

	private ActivityNetworkSolver ans = null;
	private int componentPeisID;
	private int cpmPeisID;
	private Logger logger = MetaCSPLogging.getLogger(this.getClass());

	public RobotEraDispatchingFunction(String component, ActivityNetworkSolver ans, int componentPeisID, int cpmPeisID) {
		super(component);
		this.ans = ans;
		this.componentPeisID = componentPeisID;
		this.cpmPeisID = cpmPeisID;
	}

	
	@Override
	public void dispatch(final SymbolicVariableActivity act) {
		String activitySymbol = act.getSymbolicVariable().getSymbols()[0];
		String activityParam = null;

		if (activitySymbol.indexOf("(") != -1) {
			activityParam = activitySymbol.substring(activitySymbol.indexOf("(")+1, activitySymbol.indexOf(")"));
			activitySymbol = activitySymbol.substring(0, activitySymbol.indexOf("("));
			
			//Check if this is an output State. If it is, set it to the param.
			if(activitySymbol.contains("state") && activitySymbol.startsWith("out."))
			{
				PeisJavaMT.peisjava_setStringTuple(activitySymbol, activityParam);
				this.finish(act);
			}

		}

		final String baseTupleName = this.component + "." + activitySymbol;
		final String commandTupleName = "out." + baseTupleName + ".command";
		final String stateTupleName = "in." + baseTupleName + ".state";
		final String parTupleName = "out." + baseTupleName + ".parameters";
		final String goalIDTupleName = "out." + baseTupleName + ".goalID";

		final DispatchingFunction thisDf = this;
		
		PeisJavaMT.peisjava_setMetaTuple(cpmPeisID, stateTupleName, componentPeisID, "out." + baseTupleName + ".state");
		
		try { PeisJavaMT.peisjava_setStringTupleIndirectly(cpmPeisID, stateTupleName, "IDLE"); }
		catch (PeisTupleException e) { e.printStackTrace(); }

		PeisJavaMT.peisjava_subscribeIndirectly(stateTupleName);
		
		logger.info("Waiting for action's state to be IDLE");
		while(true) {
			PeisTuple tup = PeisJavaMT.peisjava_getTupleIndirectly(cpmPeisID, stateTupleName);
			if (null != tup) {
				if (tup.getStringData().equals("IDLE")) {
			    		logger.info("State is IDLE.");
					break;
				} else {
			    		logger.info("State is not IDLE. Trying to set it to IDLE now.");
					try { Thread.sleep(100); }
					catch (InterruptedException e) { e.printStackTrace(); }
					try { PeisJavaMT.peisjava_setStringTupleIndirectly(cpmPeisID, stateTupleName, "IDLE"); }
					catch (PeisTupleException e) { e.printStackTrace(); }
				}
			} else {
				try { Thread.sleep(100); }
				catch (InterruptedException e) { e.printStackTrace(); }
				logger.info("Waiting for state tuple!");
			}
		}
		
		logger.info("Despatching " + baseTupleName + " now.");

		PeisJavaMT.peisjava_setMetaTuple(componentPeisID, "in." + baseTupleName + ".command", cpmPeisID, commandTupleName);
        PeisJavaMT.peisjava_setStringTuple(commandTupleName,"ON");
		if (activityParam != null && !activityParam.trim().equals("")) {
			PeisJavaMT.peisjava_setMetaTuple(componentPeisID, "in." + baseTupleName + ".parameters", cpmPeisID, parTupleName);
            PeisJavaMT.peisjava_setStringTuple(parTupleName,activityParam.trim());
		}
		PeisJavaMT.peisjava_setStringTuple(goalIDTupleName,""+act.getID());
		
		new Thread() {
		    public void run() {
			while(true) {
			    try { Thread.sleep(100); }
			    catch (InterruptedException e) { e.printStackTrace(); }
			    PeisTuple tup = PeisJavaMT.peisjava_getTupleIndirectly(cpmPeisID, stateTupleName);							
			    if (tup != null) {
				if ( tup.getStringData().equals("COMPLETED") ) {
				    logger.info("Action " + baseTupleName + " is completed.");
				    thisDf.finish(act);
				    PeisJavaMT.peisjava_setStringTuple(commandTupleName, "OFF");
				    try { PeisJavaMT.peisjava_setStringTupleIndirectly(cpmPeisID, stateTupleName, "IDLE"); }
				    catch (PeisTupleException e) { e.printStackTrace(); }
				    break;
				} else if ( tup.getStringData().equals("FAILED") ) {
				    try { PeisJavaMT.peisjava_setStringTupleIndirectly(cpmPeisID, stateTupleName, "IDLE"); }
				    catch (PeisTupleException e) { e.printStackTrace(); }
				}
			    }
			}
		    }
		}.start();
		
		logger.info("Waiting for Status " + baseTupleName);
		act.getVariable().setMarking("Forgettable");
	}

	@Override
	public boolean skip(SymbolicVariableActivity act) {
		// TODO Auto-generated method stub
		return false;
	}

}
