set xlabel "Iteration"

#set ylabel "# Variables"
#plot 'CPM-stats-ScenarioTwo-NO_OPT-2015-07-01-11-58-39.log' using 1 with l title "NO-OPT", 'CPM-stats-ScenarioTwo-OPT-2015-07-01-11-49-06.log' using 1 with l title "OPT"

#set ylabel "# Constraints"
#plot 'CPM-stats-ScenarioTwo-NO_OPT-2015-07-01-11-58-39.log' using 2 with l title "NO-OPT", 'CPM-stats-ScenarioTwo-OPT-2015-07-01-11-49-06.log' using 2 with l title "OPT"

set ylabel "# Solving time [msec]"
plot 'CPM-stats-ScenarioTwo-NO_OPT-2015-07-01-11-58-39.log' using 3 with l title "NO-OPT", 'CPM-stats-ScenarioTwo-OPT-2015-07-01-11-49-06.log' using 3 with l title "OPT"
