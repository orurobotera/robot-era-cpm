##################
# Reserved words #
#################################################################
#                                                               #
#   Head                                                        #
#   Resource                                                    #
#   Sensor                                                      #
#   Actuator                                                    #
#   ContextVariable                                             #
#   SimpleOperator                                              #
#   Domain                                                      #
#   Constraint                                                  #
#   RequiredState                                               #
#   AchievedState                                               #
#   RequiredResource                                            #
#   All AllenIntervalConstraint types                           #
#   '[' and ']' should be used only for constraint bounds       #
#   '(' and ')' are used for parsing                            #
#                                                               #
#################################################################

(Domain OutdoorWalking)



# sensor tuples on CAM
(Sensor in.goal.type)
(Sensor in.goal.args)

# locations used by Dustcart
(Sensor home.pos.gps)

# desired states (goals) should be inferred for doro
(ContextVariable inference.goal.type)

#############
# Actuators #
#############

# doro's states cannot occur in the past (they are actions to be executed)
(Actuator doro)
(Actuator ws)
(Actuator dustcart)


######################
# Planning operators #
######################

# User calls doro
(SimpleOperator
 (Head inference.goal.type::REACH_USER_REQUEST)
 (RequiredState req0 in.goal.type::REACH_USER_REQUEST)
 (RequiredState req1 doro::moveto(user1))
 (RequiredState req3 doro::dock(USER1))
 (RequiredState req4 ws::interaction_doro(USER_SERVICE_REQUEST))
 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req3))
 (Constraint Duration[3000,INF](req4))
 (Constraint Starts[10,10](Head,req0))
 (Constraint Meets(req1,req3))
 (Constraint Meets(req3,req4))
)

# User wants to walk outside with the help of dustcart
(SimpleOperator
 (Head inference.goal.type::OUTDOOR_WALKING)

 (RequiredState req0 in.goal.type::OUTDOOR_WALKING)

 (RequiredState req1 dustcart::moveto(home.pos.gps))
 (RequiredState req2 dustcart::startgui(WALKSUPPORT))


 (Constraint Duration[3000,INF](req1))
 (Constraint Duration[3000,INF](req2))

 (Constraint Starts[10,10](Head,req0))

 (Constraint Meets(req1,req2))

)
