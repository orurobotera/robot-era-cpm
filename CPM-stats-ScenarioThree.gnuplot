set xlabel "Iteration"

#set ylabel "# Variables"
#plot 'CPM-stats-ScenarioThree-NO_OPT-2015-07-01-12-42-05.log' using 1 with l title "NO-OPT", 'CPM-stats-ScenarioThree-OPT-2015-07-01-12-34-44.log' using 1 with l title "OPT"

#set ylabel "# Constraints"
#plot 'CPM-stats-ScenarioThree-NO_OPT-2015-07-01-12-42-05.log' using 2 with l title "NO-OPT", 'CPM-stats-ScenarioThree-OPT-2015-07-01-12-34-44.log' using 2 with l title "OPT"

set ylabel "# Solving time [msec]"
plot 'CPM-stats-ScenarioThree-NO_OPT-2015-07-01-12-42-05.log' using 3 with l title "NO-OPT", 'CPM-stats-ScenarioThree-OPT-2015-07-01-12-34-44.log' using 3 with l title "OPT"
