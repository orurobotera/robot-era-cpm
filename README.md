The Robot-Era Configuration Planning Module (CPM) is a constraint-based planner and executor based on the [Meta-CSP Framework](http://metacsp.org). It is based on the principles of constraint-based temporal planning (or timeline-based planning (see, e.g., [[Fratini, Pecora, Cesta, 2008](http://www.diva-portal.org/smash/record.jsf?pid=diva2%3A540638&dswid=3375)]). The CPM integrates context inference and planning to provide a so-called "proactive" planner which can generate its own goals depending on inferred context, as well as the plans to achieve them [[Pecora et al., 2012](http://www.aass.oru.se/Research/Learning/publications/2012/Pecora_etal_2012-JAISE-A_Constraint_Based_Approach_for_Proactive_Context_Aware_Human_Support.pdf)]. The CPM is typically used online, in a continuous loop which alternates context inference, planning, action dispatching, and feedback from execution. The CPM provides a simple state-variable based domain description language for modeling rules for context inference as well as operators for goal achievement. The planner also natively supports reusable resources, providing an integrated planning and scheduling system.  

**Use in Robot-Era.** CPM is used in the [Robot-Era](http://www.robot-era.eu/robotera/) project to dynamically generate plans for the entire Robot-Era system in order to realize a given service in the current context. It is also responsible for controlling the execution of these plans, by dispatching the relevant commands to the different components of the system at the right time, and monitoring the execution of these commands. The CPM version 1.1 was described in Deliverable D3.2, and was used in all the services tested in the first experimental loop, providing the Robot-Era system with the ability to run each service in a fully autonomous way. Version 2.0 was described in Deliverable 3.4. In addition to a general code refactoring, this version introduces Simplified and extended domain description language, to enable more complex domains needed for the open-ended scenarios in the second experimental loop. Also, it provides the ability to dynamically add goals, and to adapt the current plan to the new goals.

Version 2.0 of the CPM was used in Robot-Era's second experimental loop to plan for an ecology of robots, sensors, and other ubiquitous devices. These included pressure sensors under chairs, infra-red presence sensors in rooms, an elevator, two robots, and human-robot interaction services. Three domains implementing the 12 Robot-Era services were developed (included in this repository), and the CPM was used to test the services with 20 elderly people. The system operated for over 65 hours, controlling the delivery of 218 service invocations.

# Installation #

To compile the CPM, use the included Gradle distribution:

```
#!bash
./gradlew install
```

To get the Eclipse project pre-configured:

```
#!bash
./gradlew eclipse
```

# Running #

Run a fake executor for the components used in the scenario - in doubt,
load all of them (doro, coro, dustcart, elevator and webserver) as follows:

```
#!bash
cd scripts
./fakeExecutorDoro.sh & ./fakeExecutorCoro.sh & ./fakeExecutorDustcart.sh & ./fakeExecutorElevator.sh & ./fakeExecutorWS.sh
```

Run a fake CAM, and optionally post a goal on the command line, e.g.:

```
#!bash
cd scripts
./fakeCAM.sh "REACH_USER_REQUEST NONE NONE"
```

Post new goals in the fake CAM by setting the "out.goal.type" tuple of the fakeCAM (which has PEIS ID = 9898).

Now you can run the CPM, specifying the appropriate domain <domain-name>. For the Robot-Era tests,
<domain-name> must be one of ("ScenarioOne" or "ScenarioTwo" or "ScenarioThree").  E.g.,

```
#!bash
./gradlew run -PappArgs="['ScenarioOne']"
```

If you want to use your own/another domain, place the file <domain-name>.ddl
in the directory "domains" and run the planner as follows:

```
#!bash
./gradlew run -PappArgs="['<domain_name>']"
```

Note that the directory and suffix of the domain file are default and should
not be specified on the command line.

The CPM can also be run with an optional second argument that specifies
whether the planner should use the forgetting mechanism. This mechanism is
enabled by default. Issue the command

```
#!bash
./gradlew run
```

to get the full usage specification. 

# Other programs #

Other programs are included in this distribution: (1) a "fake executor" and
(2) a benchmark automation program. The first program is invoked as follows:

```
#!bash
./gradlew run -DmainClassName="robotEraScenario.ProcessStarter"
```

This program starts a separate PEIS component (process) for each real component
of the Robot-Era system. It takes as input the names of tuples that would be
published by the real "Exekutor". These inputs are given in the files ending with
suffix ".fe" in the scripts directory, e.g., "scripts/doro.fe".

The benchmark automation program is started as follows:

```
#!bash
./gradlew run -DmainClassName="robotEraScenario.BenchmarkAutomation" -PappArgs="['domains/ScenarioOne.goals']" 
```

This program posts on component with ID 9898 (the CAM in the Robot-Era system) a
random goal among those specified in the file given as argument.